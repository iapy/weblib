
#include <weblib/server.hpp>
#include <weblib/handler.hpp>
#include <tester/metacg.hpp>
#include "lib/client.hpp"

#include <cg/host.hpp>
#include <cg/service.hpp>

BOOST_FIXTURE_TEST_SUITE(RequestWriter, tester::Metacg)

constexpr char DATA[] = {"This is 32 bytes length message\0"};
static_assert(sizeof(DATA) - 1 == 32);

constexpr std::size_t COUNT = 16 * 1024 * 1024 / (sizeof(DATA) - 1);

void router(crow::SimpleApp &app)
{
    CROW_ROUTE(app, "/post").methods("POST"_method)([](crow::request const &request){
        return request.body + request.get_header_value(
            boost::beast::http::to_string(
                net::http::field::authorization));
    });

    CROW_ROUTE(app, "/stop")([&app]{
        app.stop();
        return "Stopped";
    });
}

void client(weblib::Client &client)
{
    {
        std::ostringstream ss;
        std::optional<weblib::Client::Raw> raw;

        auto uploader = client(weblib::method::post, weblib::http(TEST_PORT), "/post", COUNT * (sizeof(DATA) - 1));
        for(std::size_t i = 0; i < COUNT; ++i)
        {
            auto resp = uploader(&DATA[0], sizeof(DATA) - 1);
            ss.write(&DATA[0], sizeof(DATA) - 1);
            if(i == COUNT - 1)
            {
                raw = std::move(*std::move(resp));
            }
            else
            {
                BOOST_TEST(100 == resp->result_int());
            }
        }
        BOOST_TEST(!!raw);

        auto response = +std::move(*raw);
        BOOST_TEST(response.code == 200);
        BOOST_TEST(response.body == std::move(ss).str());
    }
    {
        std::ostringstream ss;
        std::optional<weblib::Client::Raw> raw;

        auto uploader = client(weblib::method::post, weblib::http(TEST_PORT), {
            {net::http::field::authorization, "Dummy"}
        }, "/post", COUNT * (sizeof(DATA) - 1));
        for(std::size_t i = 0; i < COUNT; ++i)
        {
            auto resp = uploader(&DATA[0], sizeof(DATA) - 1);
            ss.write(&DATA[0], sizeof(DATA) - 1);
            if(i == COUNT - 1)
            {
                raw = std::move(*std::move(resp));
            }
            else
            {
                BOOST_TEST(100 == resp->result_int());
            }
        }
        ss << "Dummy";
        BOOST_TEST(!!raw);

        auto response = +std::move(*raw);
        BOOST_TEST(response.code == 200);
        BOOST_TEST(response.body == std::move(ss).str());
    }
    {
        std::ostringstream ss;
        std::optional<weblib::Client::Raw> raw;

        auto uploader = client(weblib::method::post, weblib::http(TEST_PORT), {
            {net::http::field::authorization, "Dummy"}
        }, "/post", (sizeof(DATA) - 1));
        auto resp = uploader(&DATA[0], sizeof(DATA) - 1);
        ss.write(&DATA[0], sizeof(DATA) - 1);
        raw = std::move(*std::move(resp));
        ss << "Dummy";
        BOOST_TEST(!!raw);

        auto response = +std::move(*raw);
        BOOST_TEST(response.code == 200);
        BOOST_TEST(response.body == std::move(ss).str());
    }
    {
        auto response = +client(weblib::method::get, weblib::http(TEST_PORT), "/stop");
        BOOST_TEST(response.code == 200);
        BOOST_TEST(response.body == "Stopped");
    }
}

using G = cg::Graph<weblib::test::Client<0>, cg::Connect<weblib::Server, weblib::SimpleHandler>>;

BOOST_AUTO_TEST_CASE(Test)
{
    test_graph<G>(
        cg::Args<weblib::Server>("127.0.0.1", TEST_PORT, 1, false),
        cg::Args<weblib::SimpleHandler>(router),
        cg::Args<weblib::test::Client<0>>(client)
    );
}

BOOST_AUTO_TEST_SUITE_END()
