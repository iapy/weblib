#include <weblib/server.hpp>
#include <msgbus/msgbus.hpp>
#include <msgbus/service.hpp>
#include <logger/logger.hpp>
#include <cg/graph.hpp>
#include <cg/service.hpp>

struct Handler : cg::Component
{
    struct Impl
    {
        template<typename Base>
        struct Interface : Base
        {
            void setup(crow::SimpleApp &app)
            {
                CROW_ROUTE(app, "/bug")([]{
                    std::terminate();
                    return crow::response{crow::status::INTERNAL_SERVER_ERROR};
                });
            }
        };
    };

    using Ports = ct::map<ct::pair<weblib::tag::Handler, Impl>>;
};

int main()
{
    using Graph = cg::Graph<cg::Connect<weblib::Server, Handler>>;
    auto host = cg::Host<Graph>(cg::Args<weblib::Server>(
        "127.0.0.1", TEST_PORT, 1, true
    ));

    return cg::Service(host);
}
