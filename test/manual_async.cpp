#include <weblib/async/server.hpp>
#include <msgbus/msgbus.hpp>
#include <msgbus/service.hpp>
#include <logger/logger.hpp>
#include <cg/graph.hpp>
#include "test_templates.hpp"

struct Handler : cg::Component
{
    template<typename Resolver>
    struct Types
    {
        static constexpr std::size_t SIZE = 2 * 1024 * 1024;
        struct Context
        {
            auto net()        { return net::asio::buffer(*buf); }
            auto const &raw() { return *buf; }
            void use_buffer() { buf.emplace(); }
            bool has_response {false};
        private:
            std::optional<std::array<char, SIZE>> buf;
        };
    };

    struct Impl
    {
        template<typename Base>
        struct Interface : Base
        {
            template<typename Context>
            bool authenticate(Context context)
            {
                if(auto auth = context->header(net::http::field::authorization); auth)
                {
                    return !std::strcmp(auth, "Basic password\r\n");
                }
                return false;
            }

            template<typename Context>
            void handle(weblib::async::method::get, Context context)
            {
                auto stream = context->stream();
                weblib::test::body(stream, std::vector<int>{1, 2, 3});
                context->status(net::http::status::ok);
            }

            template<typename Context>
            void handle(weblib::async::method::post, Context context)
            {
                context->body([context](net::beast::error_code ec, auto &body){
                    std::istream stream(&body);
                    context->stream() << stream.rdbuf();
                    context->status(net::http::status::ok);
                    context->finish();
                });
            }

            template<typename Context>
            void handle(weblib::async::method::put, Context context)
            {
                context->use_buffer();
                context->body(context->net(), [context](net::beast::error_code ec, std::size_t cn) -> bool {
                    auto const &raw = context->raw();
                    context->stream().write(raw.data(), cn);
                    if(ec == net::http::error::end_of_stream)
                    {
                        context->status(net::http::status::ok);
                        context->finish();
                    }
                    return true;
                });
            }

            template<typename Context>
            void handle(weblib::async::method::patch, Context context)
            {
                context->use_buffer();
                context->length(*context->length());
                context->body(context->net(), [context](net::beast::error_code ec, std::size_t cn, auto &&next) {
                    using Next = std::decay_t<decltype(next)>;
                    if(!std::exchange(context->has_response, true))
                    {
                        context->status(net::http::status::ok);
                        context->send([context, next=std::forward<Next>(next), ec, cn](net::beast::error_code e, std::size_t) mutable {
                            context->send(context->net(), cn, [context, next=std::forward<Next>(next), ec](net::beast::error_code e, std::size_t) mutable {
                                if(ec && ec != net::http::error::end_of_stream)
                                {
                                    context->error(e);
                                }
                                else
                                {
                                    next(true);
                                }
                            });
                        });
                    }
                    else
                    {
                        context->send(context->net(), cn, [context, next=std::forward<Next>(next), ec](net::beast::error_code e, std::size_t) mutable {
                            if(ec && ec != net::http::error::end_of_stream)
                            {
                                context->error(e);
                            }
                            else
                            {
                                next(true);
                            }
                        });
                    }
                });
            }

            template<typename Context>
            void handle(weblib::async::method::propfind, Context context)
            {
                context->body([context](net::beast::error_code ec, auto &body){
                    std::istream stream(&body);
                    std::string data{
                        std::istreambuf_iterator<char>(stream),
                        std::istreambuf_iterator<char>()};
                    context->stream() << data;
                    context->status(net::http::status::ok);
                    context->finish();
                });
            }
        };
    };

    template<typename>
    struct State
    {
        std::filesystem::path self;
    };

    using Ports = ct::map<ct::pair<weblib::tag::async::Handler, Impl>>;
};

int main(int argc, char **argv)
{
    using G = cg::Graph<
        msgbus::Clients<weblib::async::Server, logger::Logger>,
        cg::Connect<weblib::async::Server, Handler>
    >;
    const char *host = argc > 1 ? argv[1] : "127.0.0.1";
    return msgbus::Service<G>(cg::Args<weblib::async::Server>(std::string{host}, 12345), cg::Args<Handler>(argv[0]));
}
