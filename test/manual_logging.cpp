#include <weblib/server.hpp>

#include <msgbus/msgbus.hpp>
#include <msgbus/service.hpp>

int main(int argc, char **argv)
{
    using Graph = cg::Graph
    <
        cg::Connect<weblib::Server, logger::Logger>,
        cg::Connect<logger::Logger, msgbus::Msgbus>
    >;

    return msgbus::Service<Graph>(
        cg::Args<weblib::Server>(
            "127.0.0.1", 2403, 1, true
        ),
        cg::Args<logger::Logger>(std::map<std::string, std::string>{
            {"path", std::string{argv[0]} + ".log"}
        })
    );
}
