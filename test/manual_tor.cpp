#include <weblib/agent.hpp>
#include <iostream>

int main(int argc, char **argv)
{
    weblib::Agent agent;
    agent.set("curl/8.6.0");
    agent.tor("/etc/tor/torrc");

    auto response = +agent(weblib::method::get, argv[1]);
    std::cout << response.code << '\n';
    std::cout << response.body << '\n';
    return 0;
}
