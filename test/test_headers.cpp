#include <weblib/server.hpp>
#include <weblib/handler.hpp>
#include <tester/metacg.hpp>
#include "lib/client.hpp"

#include <cg/host.hpp>
#include <cg/service.hpp>

BOOST_FIXTURE_TEST_SUITE(Headers, tester::Metacg)

void router(crow::SimpleApp &app)
{
    CROW_ROUTE(app, "/")([](crow::request const &request){
        return request.get_header_value(weblib::header(net::http::field::authorization));
    });

    CROW_ROUTE(app, "/").methods("POST"_method)([](crow::request const &request){
        return "post:" + request.get_header_value(weblib::header(net::http::field::authorization)) + ":" + request.body;
    });

    CROW_ROUTE(app, "/stop")([&app]{
        app.stop();
        return "Stopped";
    });
}

void client(weblib::Client &client)
{
    using namespace std::string_literals;
    {
        auto response = +client(weblib::method::get, weblib::http(TEST_PORT), "/");
        BOOST_TEST(response.code == 200);
        BOOST_TEST(response.body == "");
    }
    {
        auto response = +client(weblib::method::post, weblib::http(TEST_PORT), "/", "foobar"s);
        BOOST_TEST(response.code == 200);
        BOOST_TEST(response.body == "post::foobar");
    }
    {
        auto response = +client(weblib::method::get, weblib::http(TEST_PORT), {
            {net::http::field::authorization, "Dummy"}
        }, "/");
        BOOST_TEST(response.code == 200);
        BOOST_TEST(response.body == "Dummy");
    }
    {
        auto response = +client(weblib::method::post, weblib::http(TEST_PORT), {
            {net::http::field::authorization, "Dummy"}
        }, "/", "foobar"s);
        BOOST_TEST(response.code == 200);
        BOOST_TEST(response.body == "post:Dummy:foobar");
    }
    client.headers([](weblib::Client::Request &request, net::http::verb verb, std::string const &path){
        request.set(net::http::field::authorization, verb == net::http::verb::get ? "get" : "post");
    });
    {
        auto response = +client(weblib::method::get, weblib::http(TEST_PORT), "/");
        BOOST_TEST(response.code == 200);
        BOOST_TEST(response.body == "get");
    }
    {
        auto response = +client(weblib::method::post, weblib::http(TEST_PORT), "/", "foobar"s);
        BOOST_TEST(response.code == 200);
        BOOST_TEST(response.body == "post:post:foobar");
    }
    {
        auto response = +client(weblib::method::get, weblib::http(TEST_PORT), "/stop");
        BOOST_TEST(response.code == 200);
        BOOST_TEST(response.body == "Stopped");
    }
}

using G = cg::Graph<weblib::test::Client<0>, cg::Connect<weblib::Server, weblib::SimpleHandler>>;

BOOST_AUTO_TEST_CASE(Test)
{
    test_graph<G>(
        cg::Args<weblib::Server>("127.0.0.1", TEST_PORT, 1, false),
        cg::Args<weblib::SimpleHandler>(router),
        cg::Args<weblib::test::Client<0>>(client)
    );
}

BOOST_AUTO_TEST_SUITE_END()

