#include <weblib/client.hpp>
#include <tester/tester.hpp>
#include <jsonio/struct.hpp>

BOOST_AUTO_TEST_SUITE(Https)

JSON_STRUCT(Response, (
    (std::string, ip),
    (std::string, cc),
    (std::string, country)
));

BOOST_AUTO_TEST_CASE(Test)
{
    weblib::Client client{"api.myip.com"};

    auto response = +client(weblib::method::get, weblib::https, "/");
    BOOST_TEST(200 == response.code);

    auto data = response.json<Response>();
    BOOST_TEST(!data.ip.empty());
    BOOST_TEST(!data.cc.empty());
    BOOST_TEST(!data.country.empty());
}

BOOST_AUTO_TEST_SUITE_END()
