
#include <weblib/server.hpp>
#include <weblib/handler.hpp>
#include <tester/metacg.hpp>
#include "lib/client.hpp"

#include <cg/host.hpp>
#include <cg/service.hpp>

#include <random>

BOOST_FIXTURE_TEST_SUITE(LongHeader, tester::Metacg)

std::string cookie;

void router(crow::SimpleApp &app)
{
    CROW_ROUTE(app, "/")([]{
        crow::response response{crow::status::OK};
        response.set_header(weblib::header(net::http::field::cookie), cookie);
        response.body = "Hello";
        return response;
    });

    CROW_ROUTE(app, "/stop")([&app]{
        app.stop();
        return "Stopped";
    });
}

void client(weblib::Client &client)
{
    {
        auto response = client(weblib::method::get, weblib::http(TEST_PORT), "/").as<crow::response>();
        BOOST_TEST(response.code == 504);
        BOOST_TEST(response.get_header_value(weblib::header(net::http::field::cookie)) != cookie);
    }
    weblib::Client::HLIMIT = 20 * 1024;
    {
        auto response = client(weblib::method::get, weblib::http(TEST_PORT), "/").as<crow::response>();
        BOOST_TEST(response.code == 200);
        BOOST_TEST(response.body == "Hello");
        BOOST_TEST(response.get_header_value(weblib::header(net::http::field::cookie)) == cookie);
    }
    {
        auto response = +client(weblib::method::get, weblib::http(TEST_PORT), "/stop");
        BOOST_TEST(response.code == 200);
        BOOST_TEST(response.body == "Stopped");
    }
}

using G = cg::Graph<weblib::test::Client<0>, cg::Connect<weblib::Server, weblib::SimpleHandler>>;

BOOST_AUTO_TEST_CASE(Test)
{
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_int_distribution<char> distrib('a', 'z');

    cookie.resize(10 * 1024);
    for(char &ch : cookie)
    {
        ch = distrib(gen);
    }

    test_graph<G>(
        cg::Args<weblib::Server>("127.0.0.1", TEST_PORT, 1, false),
        cg::Args<weblib::SimpleHandler>(router),
        cg::Args<weblib::test::Client<0>>(client)
    );
}

BOOST_AUTO_TEST_SUITE_END()
