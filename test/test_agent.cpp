#include <weblib/agent.hpp>
#include <weblib/server.hpp>
#include <weblib/handler.hpp>
#include <tester/metacg.hpp>

#include <algorithm>
#include <sstream>
#include <vector>
#include <map>

BOOST_FIXTURE_TEST_SUITE(Agent, tester::Metacg)

JSON_STRUCT(IP, (
    (std::string, ip),
    (std::string, cc),
    (std::string, country)
));

BOOST_AUTO_TEST_CASE(Simple)
{
    weblib::Agent agent;
    {
        auto response = +agent(weblib::method::get, "https://api.myip.com");
        BOOST_TEST(200 == response.code);

        auto data = response.json<IP>();
        BOOST_TEST(!data.ip.empty());
        BOOST_TEST(!data.cc.empty());
        BOOST_TEST(!data.country.empty());
    }
    {
        auto response = +agent(weblib::method::get, "http://google.com");
        BOOST_TEST(301 == response.code);
    }
}

void router(crow::SimpleApp &app)
{
    CROW_ROUTE(app, "/")([](crow::request const &request){
        return crow::response(crow::status::OK, request.get_header_value(weblib::header(net::http::field::user_agent)));
    });

    CROW_ROUTE(app, "/").methods("POST"_method)([](crow::request const &request){
        return crow::response(crow::status::OK, "post:" + request.get_header_value(weblib::header(net::http::field::user_agent)) + ":" + request.body);
    });

    CROW_ROUTE(app, "/stop")([&app]{
        app.stop();
        return "Stopped";
    });
}

struct Client : cg::Component
{
    struct Main
    {
        template<typename Base>
        struct Interface : Base
        {
            int run()
            {
                auto a = +this->state.agent(weblib::method::get, "http://127.0.0.1:" + std::to_string(TEST_PORT));
                BOOST_TEST(200 == a.code);
                BOOST_TEST("Foo" == a.body);

                auto b = +this->state.agent(weblib::method::post, "http://127.0.0.1:" + std::to_string(TEST_PORT), std::string{"foo"});
                BOOST_TEST(200 == b.code);
                BOOST_TEST("post:Foo:foo" == b.body);

                this->state.agent.set("Bar");
                auto c = +this->state.agent(weblib::method::get, "http://127.0.0.1:" + std::to_string(TEST_PORT));
                BOOST_TEST(200 == c.code);
                BOOST_TEST("Bar" == c.body);

                auto d = +this->state.agent(weblib::method::post, "http://127.0.0.1:" + std::to_string(TEST_PORT), std::string{"bar"});
                BOOST_TEST(200 == d.code);
                BOOST_TEST("post:Bar:bar" == d.body);

                auto e = +this->state.agent(weblib::method::get, "http://127.0.0.1:" + std::to_string(TEST_PORT) + "/notfound");
                BOOST_TEST(404 == e.code);

                auto f = +this->state.agent(weblib::method::get, "http://127.0.0.1:" + std::to_string(TEST_PORT) + "/stop");
                BOOST_TEST(200 == f.code);
                BOOST_TEST("Stopped" == f.body);
                return 0;
            }
        };
    };

    template<typename>
    struct State
    {
        weblib::Agent agent;
        CONFIG_DECLARE_STATE_CONSTRUCTORS(agent) {}
    };

    using Services = ct::tuple<Main>;
};

BOOST_AUTO_TEST_CASE(String)
{
    using G = cg::Graph<Client, cg::Connect<weblib::Server, weblib::SimpleHandler>>;
    test_graph<G>(
        cg::Args<weblib::Server>("127.0.0.1", TEST_PORT, 1, false),
        cg::Args<weblib::SimpleHandler>(router),
        cg::Args<Client>("{\"agent\":\"Foo\"}"_json)
    );
}

BOOST_AUTO_TEST_CASE(Tor)
{
    auto const addresses = std::vector<std::string>{
        "https://www.nytimesn7cgmftshazwhfgzm37qxb44r64ytbb2dj3x62d2lljsciiyd.onion/",
        "http://btdigggink2pdqzqrik3blmqemsbntpzwxottujilcdjfz56jumzfsyd.onion/"
    };

    weblib::Agent agent;
    for(auto const &address : addresses) {
        BOOST_REQUIRE_THROW(agent(weblib::method::get, std::string(address)), std::exception);
    }

    agent.tor("/etc/tor/torrc");
    for(auto const &address : addresses) {
        auto response = +agent(weblib::method::get, std::string(address));
        BOOST_TEST(200 == response.code);
    }
}

BOOST_AUTO_TEST_SUITE_END()

