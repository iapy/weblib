#include <weblib/client.hpp>
#include <cg/component.hpp>
#include <functional>

namespace weblib {
namespace test {

template<size_t I>
struct Client : cg::Component
{
    struct Impl
    {
        template<typename Base>
        struct Interface : Base
        {
            int run()
            {
                this->state.functor(this->state.client);
                return 0;
            }
        };
    };

    template<typename Types>
    struct State
    {
        std::function<void(weblib::Client &)> functor;
        weblib::Client client{"127.0.0.1"};
    };

    using Services = ct::tuple<Impl>;
};

} // namespace test 
} // namespace weblib
