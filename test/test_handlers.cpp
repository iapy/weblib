#include <weblib/server.hpp>
#include <weblib/handler.hpp>
#include <tester/metacg.hpp>
#include "lib/client.hpp"

#include <cg/bind.hpp>
#include <cg/host.hpp>
#include <cg/service.hpp>

BOOST_FIXTURE_TEST_SUITE(Handlers, tester::Metacg)

void router(crow::SimpleApp &app)
{
    CROW_ROUTE(app, "/stop")([&app]{
        app.stop();
        return "Stopped";
    });
}

struct Handler : cg::Component
{
    struct Impl
    {
        template<typename Base>
        struct Interface : cg::Bind<Interface, Base>
        {
            void setup(crow::SimpleApp &app)
            {
                using Self = Interface<Base>;
                CROW_ROUTE(app, "/add/<int>/<int>")(this->bind(&Self::add));
                CROW_ROUTE(app, "/sub/<int>/<int>")(this->bind(&Self::sub));

                CROW_ROUTE(app, "/save/<int>")(this->bind(&Self::save));
                CROW_ROUTE(app, "/load")(this->bind(&Self::load));
            }
        private:
            std::string add(int a, int b)
            {
                return std::to_string(a + b);
            }

            std::string sub(int a, int b)
            {
                return std::to_string(a - b);
            }

            std::string save(int value)
            {
                this->state = value;
                return std::to_string(value);
            }

            std::string load()
            {
                return std::to_string(this->state);
            }
        };
    };

    template<typename>
    using State = int;

    using Ports = ct::map<ct::pair<weblib::tag::Handler, Impl>>;
};

void client(weblib::Client &client)
{
    {
        auto response = +client(weblib::method::get, weblib::http(TEST_PORT), "/add/31/11");
        BOOST_TEST(response.code == 200);
        BOOST_TEST(response.body == "42");
    }
    {
        auto response = +client(weblib::method::get, weblib::http(TEST_PORT), "/sub/31/11");
        BOOST_TEST(response.code == 200);
        BOOST_TEST(response.body == "20");
    }
    {
        auto response = +client(weblib::method::get, weblib::http(TEST_PORT), "/save/2310");
        BOOST_TEST(response.code == 200);
        BOOST_TEST(response.body == "2310");
    }
    {
        auto response = client(weblib::method::get, weblib::http(TEST_PORT), "/load").as<weblib::Client::Response>();
        BOOST_TEST(response.code == 200);
        BOOST_TEST(response.body == "2310");
    }
    {
        auto response = client(weblib::method::get, weblib::http(TEST_PORT), "/stop").as<weblib::Client::Response>();
        BOOST_TEST(response.code == 200);
        BOOST_TEST(response.body == "Stopped");
    }
}

using G = cg::Graph<weblib::test::Client<0>, cg::Connect<weblib::Server, cg::Group<
    weblib::SimpleHandler,
    Handler
>>>;

BOOST_AUTO_TEST_CASE(Test)
{
    test_graph<G>(
        cg::Args<weblib::Server>("127.0.0.1", TEST_PORT, 1, false),
        cg::Args<weblib::SimpleHandler>(router),
        cg::Args<weblib::test::Client<0>>(client)
    );
}

BOOST_AUTO_TEST_SUITE_END()
