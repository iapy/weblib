#include <weblib/client.hpp>
#include <tester/tester.hpp>
#include <boost/process.hpp>

#include <condition_variable>
#include <filesystem>
#include <chrono>
#include <thread>
#include <mutex>

BOOST_AUTO_TEST_SUITE(Buggy)

BOOST_AUTO_TEST_CASE(Test)
{
    static constexpr std::size_t ITERATIONS{5};

    volatile bool started{false};
    std::condition_variable cv;
    std::mutex mu;

    std::thread thread([&]{
        using namespace std::literals::chrono_literals;
        namespace bp = boost::process;

        auto const path = std::filesystem::path(BUILD_DIR) / "manual_buggy";
        for(std::size_t it = 0; it < ITERATIONS; ++it)
        {
            bp::child server(path.u8string());
            std::this_thread::sleep_for(1s);

            std::unique_lock lock(mu);
            started = true;
            cv.notify_one();
            lock.unlock();

            server.wait();
            BOOST_REQUIRE(0 != server.exit_code());

            std::this_thread::sleep_for(1s);
        }
    });

    weblib::Client client{"127.0.0.1"};
    for(std::size_t it = 0; it < ITERATIONS; ++it)
    {
        std::unique_lock lock(mu);
        cv.wait(lock, [&]{
            return started;
        });
        started = false;

        auto response = +client(weblib::method::get, weblib::http(TEST_PORT), "/bug");
        BOOST_REQUIRE(504 == response.code);
    }

    thread.join();
}

BOOST_AUTO_TEST_SUITE_END()
