#include <boost/asio/buffers_iterator.hpp>
#include <weblib/async/client.hpp>
#include <boost/hana.hpp>

class Context : public std::enable_shared_from_this<Context>
{
public:
    Context(weblib::async::Client::Executor const &ex, net::tcp::resolver::results_type const &rt)
    : client_(ex, rt)
    {
    }

    void start()
    {
        auto &req = client_.request(net::http::verb::get, "/");
        req.set(net::http::field::authorization, "Basic password");
        client_.send([&req, self=Context::shared_from_this()](net::beast::error_code ec, std::size_t bc){
            self->client_.recv([self](net::beast::error_code ec, std::size_t bc) {
            });
        });
    }

private:
    net::asio::streambuf buffer_;
    weblib::async::Client client_;
};

int main(int argc, char **argv)
{
    net::asio::io_context ioc{1};
    net::tcp::resolver resolv{ioc};
    std::make_shared<Context>(resolv.get_executor(), resolv.resolve("127.0.0.1", "12345"))->start();
    return 0 < ioc.run();
}
