#include <weblib/agent.hpp>
#include <iostream>
#include <sstream>

int main(int argc, char **argv)
{
#if 0
    auto fetcher = weblib::Agent{};
    if(!strcmp(argv[1], "GET"))
    {
        auto response = fetcher(weblib::method::get, argv[2]);
        std::cout << response.code << '\n';
        std::cout << response.body << '\n';
    }
    else if(!strcmp(argv[1], "POST"))
    {
        auto response = fetcher(weblib::method::post, argv[2], []{
            std::ostringstream ss;
            ss << std::cin.rdbuf();
            return ss.str();
        }());
        std::cout << response.code << '\n';
        std::cout << response.body << '\n';
    }
#endif
    return 0;
}
