#include <weblib/agent.hpp>
#include <tester/tester.hpp>

#include <algorithm>
#include <sstream>
#include <vector>
#include <map>

BOOST_AUTO_TEST_SUITE(URL)

BOOST_AUTO_TEST_CASE(Negative)
{
    BOOST_TEST(!weblib::Agent::parse(""));
    BOOST_TEST(!weblib::Agent::parse("/"));
    BOOST_TEST(!weblib::Agent::parse("//"));
    BOOST_TEST(!weblib::Agent::parse("ftp://"));
    BOOST_TEST(!weblib::Agent::parse("http://"));
    BOOST_TEST(!weblib::Agent::parse("https://"));
    BOOST_TEST(!weblib::Agent::parse("https:///"));
    BOOST_TEST(!weblib::Agent::parse("https://:"));
    BOOST_TEST(!weblib::Agent::parse("https://?"));
}

BOOST_AUTO_TEST_CASE(Positive)
{
    std::string url;
    std::optional<weblib::Agent::URL> parsed;
    
    parsed = weblib::Agent::parse(url = "http://127.0.0.1");
    BOOST_REQUIRE(parsed);

    BOOST_TEST(std::get<0>(*parsed) == false);
    BOOST_TEST(std::get<1>(*parsed) == "127.0.0.1");
    BOOST_TEST(std::get<2>(*parsed) == 80);
    BOOST_TEST(std::get<3>(*parsed) == "/");

    parsed = weblib::Agent::parse(url = "http://127.0.0.1/");
    BOOST_REQUIRE(parsed);

    BOOST_TEST(std::get<0>(*parsed) == false);
    BOOST_TEST(std::get<1>(*parsed) == "127.0.0.1");
    BOOST_TEST(std::get<2>(*parsed) == 80);
    BOOST_TEST(std::get<3>(*parsed) == "/");

    parsed = weblib::Agent::parse(url = "https://127.0.0.1");
    BOOST_REQUIRE(parsed);

    BOOST_TEST(std::get<0>(*parsed) == true);
    BOOST_TEST(std::get<1>(*parsed) == "127.0.0.1");
    BOOST_TEST(std::get<2>(*parsed) == 443);
    BOOST_TEST(std::get<3>(*parsed) == "/");

    parsed = weblib::Agent::parse(url = "https://127.0.0.1/");
    BOOST_REQUIRE(parsed);

    BOOST_TEST(std::get<0>(*parsed) == true);
    BOOST_TEST(std::get<1>(*parsed) == "127.0.0.1");
    BOOST_TEST(std::get<2>(*parsed) == 443);
    BOOST_TEST(std::get<3>(*parsed) == "/");

    parsed = weblib::Agent::parse(url = "http://127.0.0.1:88");
    BOOST_REQUIRE(parsed);

    BOOST_TEST(std::get<0>(*parsed) == false);
    BOOST_TEST(std::get<1>(*parsed) == "127.0.0.1");
    BOOST_TEST(std::get<2>(*parsed) == 88);
    BOOST_TEST(std::get<3>(*parsed) == "/");

    parsed = weblib::Agent::parse(url = "https://127.0.0.1:8443");
    BOOST_REQUIRE(parsed);

    BOOST_TEST(std::get<0>(*parsed) == true);
    BOOST_TEST(std::get<1>(*parsed) == "127.0.0.1");
    BOOST_TEST(std::get<2>(*parsed) == 8443);
    BOOST_TEST(std::get<3>(*parsed) == "/");

    parsed = weblib::Agent::parse(url = "http://127.0.0.1/foo/bar");
    BOOST_REQUIRE(parsed);

    BOOST_TEST(std::get<0>(*parsed) == false);
    BOOST_TEST(std::get<1>(*parsed) == "127.0.0.1");
    BOOST_TEST(std::get<2>(*parsed) == 80);
    BOOST_TEST(std::get<3>(*parsed) == "/foo/bar");

    parsed = weblib::Agent::parse(url = "https://127.0.0.1/foo/bar");
    BOOST_REQUIRE(parsed);

    BOOST_TEST(std::get<0>(*parsed) == true);
    BOOST_TEST(std::get<1>(*parsed) == "127.0.0.1");
    BOOST_TEST(std::get<2>(*parsed) == 443);
    BOOST_TEST(std::get<3>(*parsed) == "/foo/bar");

    parsed = weblib::Agent::parse(url = "http://127.0.0.1:88/foo/bar");
    BOOST_REQUIRE(parsed);

    BOOST_TEST(std::get<0>(*parsed) == false);
    BOOST_TEST(std::get<1>(*parsed) == "127.0.0.1");
    BOOST_TEST(std::get<2>(*parsed) == 88);
    BOOST_TEST(std::get<3>(*parsed) == "/foo/bar");

    parsed = weblib::Agent::parse(url = "https://127.0.0.1:8443/foo/bar");
    BOOST_REQUIRE(parsed);

    BOOST_TEST(std::get<0>(*parsed) == true);
    BOOST_TEST(std::get<1>(*parsed) == "127.0.0.1");
    BOOST_TEST(std::get<2>(*parsed) == 8443);
    BOOST_TEST(std::get<3>(*parsed) == "/foo/bar");

    parsed = weblib::Agent::parse(url = "http://127.0.0.1/foo/bar?baz=zzz");
    BOOST_REQUIRE(parsed);

    BOOST_TEST(std::get<0>(*parsed) == false);
    BOOST_TEST(std::get<1>(*parsed) == "127.0.0.1");
    BOOST_TEST(std::get<2>(*parsed) == 80);
    BOOST_TEST(std::get<3>(*parsed) == "/foo/bar?baz=zzz");

    parsed = weblib::Agent::parse(url = "https://127.0.0.1/foo/bar?baz=zzz");
    BOOST_REQUIRE(parsed);

    BOOST_TEST(std::get<0>(*parsed) == true);
    BOOST_TEST(std::get<1>(*parsed) == "127.0.0.1");
    BOOST_TEST(std::get<2>(*parsed) == 443);
    BOOST_TEST(std::get<3>(*parsed) == "/foo/bar?baz=zzz");

    parsed = weblib::Agent::parse(url = "http://127.0.0.1:88/foo/bar?baz=zzz");
    BOOST_REQUIRE(parsed);

    BOOST_TEST(std::get<0>(*parsed) == false);
    BOOST_TEST(std::get<1>(*parsed) == "127.0.0.1");
    BOOST_TEST(std::get<2>(*parsed) == 88);
    BOOST_TEST(std::get<3>(*parsed) == "/foo/bar?baz=zzz");

    parsed = weblib::Agent::parse(url = "https://127.0.0.1:8443/foo/bar?baz=zzz");
    BOOST_REQUIRE(parsed);

    BOOST_TEST(std::get<0>(*parsed) == true);
    BOOST_TEST(std::get<1>(*parsed) == "127.0.0.1");
    BOOST_TEST(std::get<2>(*parsed) == 8443);
    BOOST_TEST(std::get<3>(*parsed) == "/foo/bar?baz=zzz");
    
    parsed = weblib::Agent::parse(url = "http://google.com");
    BOOST_REQUIRE(parsed);

    BOOST_TEST(std::get<0>(*parsed) == false);
    BOOST_TEST(std::get<1>(*parsed) == "google.com");
    BOOST_TEST(std::get<2>(*parsed) == 80);
    BOOST_TEST(std::get<3>(*parsed) == "/");

    parsed = weblib::Agent::parse(url = "https://google.com");
    BOOST_REQUIRE(parsed);

    BOOST_TEST(std::get<0>(*parsed) == true);
    BOOST_TEST(std::get<1>(*parsed) == "google.com");
    BOOST_TEST(std::get<2>(*parsed) == 443);
    BOOST_TEST(std::get<3>(*parsed) == "/");

    parsed = weblib::Agent::parse(url = "http://google.com:88");
    BOOST_REQUIRE(parsed);

    BOOST_TEST(std::get<0>(*parsed) == false);
    BOOST_TEST(std::get<1>(*parsed) == "google.com");
    BOOST_TEST(std::get<2>(*parsed) == 88);
    BOOST_TEST(std::get<3>(*parsed) == "/");

    parsed = weblib::Agent::parse(url = "https://google.com:8443");
    BOOST_REQUIRE(parsed);

    BOOST_TEST(std::get<0>(*parsed) == true);
    BOOST_TEST(std::get<1>(*parsed) == "google.com");
    BOOST_TEST(std::get<2>(*parsed) == 8443);
    BOOST_TEST(std::get<3>(*parsed) == "/");

    parsed = weblib::Agent::parse(url = "http://google.com/foo/bar");
    BOOST_REQUIRE(parsed);

    BOOST_TEST(std::get<0>(*parsed) == false);
    BOOST_TEST(std::get<1>(*parsed) == "google.com");
    BOOST_TEST(std::get<2>(*parsed) == 80);
    BOOST_TEST(std::get<3>(*parsed) == "/foo/bar");

    parsed = weblib::Agent::parse(url = "https://google.com/foo/bar");
    BOOST_REQUIRE(parsed);

    BOOST_TEST(std::get<0>(*parsed) == true);
    BOOST_TEST(std::get<1>(*parsed) == "google.com");
    BOOST_TEST(std::get<2>(*parsed) == 443);
    BOOST_TEST(std::get<3>(*parsed) == "/foo/bar");

    parsed = weblib::Agent::parse(url = "http://google.com:88/foo/bar");
    BOOST_REQUIRE(parsed);

    BOOST_TEST(std::get<0>(*parsed) == false);
    BOOST_TEST(std::get<1>(*parsed) == "google.com");
    BOOST_TEST(std::get<2>(*parsed) == 88);
    BOOST_TEST(std::get<3>(*parsed) == "/foo/bar");

    parsed = weblib::Agent::parse(url = "https://google.com:8443/foo/bar");
    BOOST_REQUIRE(parsed);

    BOOST_TEST(std::get<0>(*parsed) == true);
    BOOST_TEST(std::get<1>(*parsed) == "google.com");
    BOOST_TEST(std::get<2>(*parsed) == 8443);
    BOOST_TEST(std::get<3>(*parsed) == "/foo/bar");

    parsed = weblib::Agent::parse(url = "http://google.com/foo/bar?baz=zzz");
    BOOST_REQUIRE(parsed);

    BOOST_TEST(std::get<0>(*parsed) == false);
    BOOST_TEST(std::get<1>(*parsed) == "google.com");
    BOOST_TEST(std::get<2>(*parsed) == 80);
    BOOST_TEST(std::get<3>(*parsed) == "/foo/bar?baz=zzz");

    parsed = weblib::Agent::parse(url = "https://google.com/foo/bar?baz=zzz");
    BOOST_REQUIRE(parsed);

    BOOST_TEST(std::get<0>(*parsed) == true);
    BOOST_TEST(std::get<1>(*parsed) == "google.com");
    BOOST_TEST(std::get<2>(*parsed) == 443);
    BOOST_TEST(std::get<3>(*parsed) == "/foo/bar?baz=zzz");

    parsed = weblib::Agent::parse(url = "http://google.com:88/foo/bar?baz=zzz");
    BOOST_REQUIRE(parsed);

    BOOST_TEST(std::get<0>(*parsed) == false);
    BOOST_TEST(std::get<1>(*parsed) == "google.com");
    BOOST_TEST(std::get<2>(*parsed) == 88);
    BOOST_TEST(std::get<3>(*parsed) == "/foo/bar?baz=zzz");

    parsed = weblib::Agent::parse(url = "https://google.com:8443/foo/bar?baz=zzz");
    BOOST_REQUIRE(parsed);

    BOOST_TEST(std::get<0>(*parsed) == true);
    BOOST_TEST(std::get<1>(*parsed) == "google.com");
    BOOST_TEST(std::get<2>(*parsed) == 8443);
    BOOST_TEST(std::get<3>(*parsed) == "/foo/bar?baz=zzz");

    parsed = weblib::Agent::parse(url = "https://www.nytimesn7cgmftshazwhfgzm37qxb44r64ytbb2dj3x62d2lljsciiyd.onion/");
    BOOST_REQUIRE(parsed);

    BOOST_TEST(std::get<0>(*parsed) == true);
    BOOST_TEST(std::get<1>(*parsed) == "www.nytimesn7cgmftshazwhfgzm37qxb44r64ytbb2dj3x62d2lljsciiyd.onion");
    BOOST_TEST(std::get<2>(*parsed) == 443);
    BOOST_TEST(std::get<3>(*parsed) == "/");

    parsed = weblib::Agent::parse(url = "https://www.nytimesn7cgmftshazwhfgzm37qxb44r64ytbb2dj3x62d2lljsciiyd.onion");
    BOOST_REQUIRE(parsed);

    BOOST_TEST(std::get<0>(*parsed) == true);
    BOOST_TEST(std::get<1>(*parsed) == "www.nytimesn7cgmftshazwhfgzm37qxb44r64ytbb2dj3x62d2lljsciiyd.onion");
    BOOST_TEST(std::get<2>(*parsed) == 443);
    BOOST_TEST(std::get<3>(*parsed) == "/");

    parsed = weblib::Agent::parse(url = "https://google.com?q=a:b/c:d");
    BOOST_REQUIRE(parsed);

    BOOST_TEST(std::get<0>(*parsed) == true);
    BOOST_TEST(std::get<1>(*parsed) == "google.com");
    BOOST_TEST(std::get<2>(*parsed) == 443);
    BOOST_TEST(std::get<3>(*parsed) == "?q=a:b/c:d");
}

BOOST_AUTO_TEST_SUITE_END()
