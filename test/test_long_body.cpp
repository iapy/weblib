#include <weblib/server.hpp>
#include <weblib/handler.hpp>
#include <tester/metacg.hpp>
#include "lib/client.hpp"

#include <cg/host.hpp>
#include <cg/service.hpp>

#include <random>

BOOST_FIXTURE_TEST_SUITE(LongBody, tester::Metacg)

constexpr char DATA[] = {"This is 32 bytes length message\0"};
static_assert(sizeof(DATA) - 1 == 32);

constexpr std::size_t COUNT = 16 * 1024 * 1024 / (sizeof(DATA) - 1);

weblib::Client *shared{nullptr};

void router(crow::SimpleApp &app)
{
    CROW_ROUTE(app, "/")([]{
        auto raw = (*shared)(weblib::method::get, weblib::http(TEST_PORT), "/body");
        BOOST_TEST(static_cast<bool>(raw.reader));
        return std::move(raw).as<crow::response>();
    });

    CROW_ROUTE(app, "/body")([]{
        crow::response response{crow::status::OK};

        response.set_header("Content-Length", std::to_string((sizeof(DATA) - 1) * COUNT + 5));
        response.set_producer([count=std::size_t(0)](net::asio::mutable_buffer &buf, boost::system::error_code &ec) mutable -> std::size_t {
            std::memcpy(buf.data(), DATA, sizeof(DATA) - 1);
            if(++count == COUNT)
            {
                std::memcpy(reinterpret_cast<char*>(buf.data()) + sizeof(DATA), "Done.", 5);
                ec = boost::asio::error::eof;
                return sizeof(DATA) + 4;
            }
            return sizeof(DATA) - 1;
        });

        return response;
    });

    CROW_ROUTE(app, "/flat")([]{
        crow::response response{crow::status::OK};

        response.set_header("Content-Length", std::to_string((sizeof(DATA) - 1) * COUNT + 5));
        response.set_producer([count=std::size_t(0)](net::asio::mutable_buffer &buf, boost::system::error_code &ec) mutable -> std::size_t {
            std::memcpy(buf.data(), DATA, sizeof(DATA) - 1);
            if(++count == COUNT)
            {
                std::memcpy(reinterpret_cast<char*>(buf.data()) + sizeof(DATA), "Done.", 5);
                ec = boost::asio::error::eof;
                return sizeof(DATA) + 4;
            }
            return sizeof(DATA) - 1;
        });

        std::string const &body = response.get_body();
        BOOST_TEST(body.size() == 5 + COUNT * (sizeof(DATA) - 1));
        BOOST_TEST(response.body.size() == 5 + COUNT * (sizeof(DATA) - 1));
        return response;
    });

    CROW_ROUTE(app, "/stop")([&app]{
        app.stop();
        return "Stopped";
    });
}

void client(weblib::Client &client)
{
    std::ostringstream ss;
    for(std::size_t i = 0; i < COUNT; ++i)
        ss.write(DATA, sizeof(DATA) - 1);
    ss << "Done.";
    std::string const expected = std::move(ss).str();

    shared = &client;
    {
        auto response = +client(weblib::method::get, weblib::http(TEST_PORT), "/body");
        BOOST_TEST(response.code == 200);
        BOOST_TEST(response.body.size() == expected.size());
    }
    {
        auto response = +client(weblib::method::get, weblib::http(TEST_PORT), "/flat");
        BOOST_TEST(response.code == 200);
        BOOST_TEST(response.body.size() == expected.size());
    }
    {
        auto response = +client(weblib::method::get, weblib::http(TEST_PORT), "/");
        BOOST_TEST(response.code == 200);
        BOOST_TEST(response.body.size() == expected.size());
    }
    {
        auto response = +client(weblib::method::get, weblib::http(TEST_PORT), "/stop");
        BOOST_TEST(response.code == 200);
        BOOST_TEST(response.body == "Stopped");
    }
}

using G = cg::Graph<weblib::test::Client<0>, cg::Connect<weblib::Server, weblib::SimpleHandler>>;

BOOST_AUTO_TEST_CASE(Test)
{
    test_graph<G>(
        cg::Args<weblib::Server>("127.0.0.1", TEST_PORT, 5, false),
        cg::Args<weblib::SimpleHandler>(router),
        cg::Args<weblib::test::Client<0>>(client)
    );
}

BOOST_AUTO_TEST_SUITE_END()
