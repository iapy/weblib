#include <weblib/server.hpp>
#include <weblib/handler.hpp>
#include <tester/metacg.hpp>
#include "lib/client.hpp"

#include <cg/host.hpp>
#include <cg/service.hpp>

BOOST_FIXTURE_TEST_SUITE(ForwardLongBody, tester::Metacg)

constexpr char DATA[] = {"This is 32 bytes length message\0"};
static_assert(sizeof(DATA) - 1 == 32);

constexpr std::size_t COUNT = 5 * 16384 / (sizeof(DATA) - 1);
static_assert(COUNT * (sizeof(DATA) - 1) == 5 * 16384);

weblib::Client *shared{nullptr};

void router(crow::SimpleApp &app)
{
    CROW_ROUTE(app, "/forwarded/<path>").methods("POST"_method)([](crow::request const &req, std::string const &path){
        return path + ':' + req.body;
    });

    CROW_ROUTE(app, "/stop")([&app]{
        app.stop();
        return "Stopped";
    });

    CROW_CATCHALL_ROUTE(app)([](crow::request request){
        request.raw_url = "/forwarded" + request.raw_url;
        return shared->forward(weblib::http(TEST_PORT), std::move(request)).as<crow::response>();
    });
}

void client(weblib::Client &client)
{
    std::ostringstream ss;
    for(std::size_t i = 0; i < COUNT; ++i)
    {
        ss.write(DATA, sizeof(DATA) - 1);
    }
    ss << "Done.";

    shared = &client;
    std::string body = std::move(ss).str();
    {
        using namespace std::string_literals;
        auto response = +client(weblib::method::post, weblib::http(TEST_PORT), "/random/path", std::string(body));
        BOOST_TEST(response.body == "random/path:" + body);
        BOOST_TEST(response.code == 200);
    }
    {
        auto response = +client(weblib::method::get, weblib::http(TEST_PORT), "/stop");
        BOOST_TEST(response.code == 200);
        BOOST_TEST(response.body == "Stopped");
    }
}

using G = cg::Graph<weblib::test::Client<0>, cg::Connect<weblib::Server, weblib::SimpleHandler>>;

BOOST_AUTO_TEST_CASE(Test)
{
    test_graph<G>(
        cg::Args<weblib::Server>("127.0.0.1", TEST_PORT, 5, false),
        cg::Args<weblib::SimpleHandler>(router),
        cg::Args<weblib::test::Client<0>>(client)
    );
}

BOOST_AUTO_TEST_SUITE_END()
