#include <weblib/client.hpp>
#include <tester/tester.hpp>

BOOST_AUTO_TEST_SUITE(Escape)

BOOST_AUTO_TEST_CASE(Test)
{
    std::string const origin = "Зара Мика";

    std::string escaped = weblib::escape(origin.begin(), origin.end());
    BOOST_TEST(escaped == "%D0%97%D0%B0%D1%80%D0%B0%20%D0%9C%D0%B8%D0%BA%D0%B0");

    std::string unescaped = weblib::unescape(escaped.begin(), escaped.end());
    BOOST_TEST(unescaped == origin);
}

BOOST_AUTO_TEST_SUITE_END()
