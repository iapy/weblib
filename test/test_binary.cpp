#include <weblib/server.hpp>
#include <weblib/handler.hpp>
#include <tester/metacg.hpp>
#include "lib/client.hpp"

#include <cg/host.hpp>
#include <cg/service.hpp>

#include <random>

BOOST_FIXTURE_TEST_SUITE(Binary, tester::Metacg)

void router(crow::SimpleApp &app)
{
    CROW_ROUTE(app, "/body").methods("POST"_method)([](crow::request const &request){
        BOOST_TEST(request.get_header_value("Content-Type") == "application/x-binary");
        BOOST_TEST(request.body == "Binary data");
        return "OK";
    });

    CROW_ROUTE(app, "/stop")([&app]{
        app.stop();
        return "Stopped";
    });
}

void client(weblib::Client &client)
{
    {
        std::string data{"Binary data"};
        auto response = +client(weblib::method::post, weblib::http(TEST_PORT), "/body", std::vector<char>(
            std::begin(data), std::end(data)
        ));
        BOOST_TEST(response.code == 200);
    }
    {
        auto response = +client(weblib::method::get, weblib::http(TEST_PORT), "/stop");
        BOOST_TEST(response.code == 200);
        BOOST_TEST(response.body == "Stopped");
    }
}

using G = cg::Graph<weblib::test::Client<0>, cg::Connect<weblib::Server, weblib::SimpleHandler>>;

BOOST_AUTO_TEST_CASE(Test)
{
    test_graph<G>(
        cg::Args<weblib::Server>("127.0.0.1", TEST_PORT, 5, false),
        cg::Args<weblib::SimpleHandler>(router),
        cg::Args<weblib::test::Client<0>>(client)
    );
}

BOOST_AUTO_TEST_SUITE_END()
