#include <weblib/server.hpp>
#include <weblib/handler.hpp>
#include <weblib/router.hpp>
#include <tester/metacg.hpp>
#include "lib/client.hpp"

#include <cg/host.hpp>
#include <cg/service.hpp>

#include <crow/middlewares/cookie_parser.h>

BOOST_FIXTURE_TEST_SUITE(Middleware, tester::Metacg)

struct Handler : cg::Component
{
    template<typename Resolver>
    struct Types
    {
        struct Middleware : crow::CookieParser
        {
            void before_handle(crow::request& req, crow::response& res, context& ctx)
            {
                crow::CookieParser::before_handle(req, res, ctx);
                if(ctx.get_cookie("Username") == std::string{})
                {
                    ctx.set_cookie("Username", "user");
                    res.end();
                }
            }

            void after_handle(crow::request& req, crow::response& res, context& ctx)
            {
                crow::CookieParser::after_handle(req, res, ctx);
            }
        };
    };

    struct Impl
    {
        template<typename Base>
        struct Interface : cg::Bind<Interface, Base>
        {
            template<typename ...Middlewares>
            void setup(crow::App<Middlewares...> &app)
            {
                WEBLIB_ROUTE(app, "/stop")([&app]{
                    app.stop();
                    return "Stopped";
                });
            }
        };
    };

    using Ports = ct::map<ct::pair<weblib::tag::Handler, Impl>>;
};

void client(weblib::Client &client)
{
    {
        auto response = +client(weblib::method::get, weblib::http(TEST_PORT), "/stop");
        BOOST_TEST(response.code == 200);
        BOOST_TEST(response.body.empty());
    }
    {
        auto response = +client(weblib::method::get, weblib::http(TEST_PORT), "/stop");
        BOOST_TEST(response.code == 200);
        BOOST_TEST(response.body == "Stopped");
    }
}

using G = cg::Graph<weblib::test::Client<0>, cg::Connect<weblib::Server, Handler>>;

BOOST_AUTO_TEST_CASE(Test)
{
    test_graph<G>(
        cg::Args<weblib::Server>("127.0.0.1", TEST_PORT, 1, false),
        cg::Args<weblib::test::Client<0>>(client)
    );
    test_graph<G>(
        cg::Args<weblib::Server>("127.0.0.1", TEST_PORT, 1, false),
        cg::Args<weblib::test::Client<0>>(client)
    );
}

BOOST_AUTO_TEST_SUITE_END()
