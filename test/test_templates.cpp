#include <boost/hana.hpp>
#include <tester/tester.hpp>
#include "test_templates.hpp"

#include <algorithm>
#include <sstream>
#include <vector>
#include <map>

BOOST_AUTO_TEST_SUITE(Templates)

BOOST_AUTO_TEST_CASE(Test)
{
    std::ostringstream ss;
    weblib::test::body(ss, std::vector<std::string>{
        "foo", "bar", "baz"
    });

    std::string s = ss.str();
    s.erase(std::remove_if(s.begin(), s.end(), ::isspace), s.end());

    BOOST_TEST(s == "<html><head><title>Title</title></head><body><table><tr><td>foo</td><td>foo</td></tr><tr><td>bar</td><td>bar</td></tr><tr><td>baz</td><td>baz</td></tr></table></body></html>");
}

BOOST_AUTO_TEST_SUITE_END()
