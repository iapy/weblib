#include <weblib/server.hpp>
#include <weblib/handler.hpp>
#include <tester/metacg.hpp>
#include "lib/client.hpp"

#include <cg/host.hpp>
#include <cg/service.hpp>

BOOST_FIXTURE_TEST_SUITE(Forward, tester::Metacg)

weblib::Client *shared{nullptr};

void router(crow::SimpleApp &app)
{
    CROW_ROUTE(app, "/forwarded/<path>").methods(
        "GET"_method,
        "POST"_method
    )([](crow::request const &req, std::string const &path){
        std::ostringstream body;
        body << crow::method_name(req.method);
        body << ":/" << path;
        auto auth = req.get_header_value(
            boost::beast::http::to_string(
                net::http::field::authorization));
        if(!auth.empty())
        {
            body << ":" << auth;
        }
        body << ":" << req.body;
        return crow::response{crow::status::OK, std::move(body).str()};
    });

    CROW_ROUTE(app, "/stop")([&app]{
        app.stop();
        return "Stopped";
    });

    CROW_CATCHALL_ROUTE(app)([](crow::request request){
        request.raw_url = "/forwarded" + request.raw_url;
        return shared->forward(weblib::http(TEST_PORT), std::move(request)).as<crow::response>();
    });
}

void client(weblib::Client &client)
{
    shared = &client;
    {
        auto response = +client(weblib::method::get, weblib::http(TEST_PORT), "/random/path");
        BOOST_TEST(response.body == "GET:/random/path:");
        BOOST_TEST(response.code == 200);
    }
    {
        auto response = +client(weblib::method::get, weblib::http(TEST_PORT), weblib::Client::Headers{
            {net::http::field::authorization, "Dummy"}
        }, "/random/path");
        BOOST_TEST(response.body == "GET:/random/path:Dummy:");
        BOOST_TEST(response.code == 200);
    }
    {
        using namespace std::string_literals;
        auto response = +client(weblib::method::post, weblib::http(TEST_PORT), "/random/path", weblib::Form{
            {"foo"s, "bar"s}
        });
        BOOST_TEST(response.body == "POST:/random/path:foo=bar");
        BOOST_TEST(response.code == 200);
    }
    {
        auto response = +client(weblib::method::get, weblib::http(TEST_PORT), "/stop");
        BOOST_TEST(response.code == 200);
        BOOST_TEST(response.body == "Stopped");
    }
}

using G = cg::Graph<weblib::test::Client<0>, cg::Connect<weblib::Server, weblib::SimpleHandler>>;

BOOST_AUTO_TEST_CASE(Test)
{
    test_graph<G>(
        cg::Args<weblib::Server>("127.0.0.1", TEST_PORT, 5, false),
        cg::Args<weblib::SimpleHandler>(router),
        cg::Args<weblib::test::Client<0>>(client)
    );
}

BOOST_AUTO_TEST_SUITE_END()
