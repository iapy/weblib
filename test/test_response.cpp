#include <weblib/server.hpp>
#include <weblib/handler.hpp>
#include <weblib/response.hpp>
#include <tester/metacg.hpp>
#include <jsonio/struct.hpp>
#include "lib/client.hpp"

#include <cg/host.hpp>
#include <cg/service.hpp>

BOOST_FIXTURE_TEST_SUITE(Response, tester::Metacg)

JSON_STRUCT(Data, (
    (std::string, key),
    (int, value)
));

void router(crow::SimpleApp &app)
{
    CROW_ROUTE(app, "/test")([]{
        return weblib::response(Data{
            .key = "foo",
            .value = 42
        });
    });

    CROW_ROUTE(app, "/stop")([&app]{
        app.stop();
        return "Stopped";
    });
}

void client(weblib::Client &client)
{
    {
        auto response = client(weblib::method::get, weblib::http(TEST_PORT), "/test").as<crow::response>();
        BOOST_TEST(response.code == 200);
        BOOST_TEST(response.get_header_value("Content-Type") == "application/json");

        auto test = jsonio::from<Data>(nlohmann::json::parse(response.body));
        BOOST_TEST("foo" == test.key);
        BOOST_TEST(42 == test.value);
    }
    {
        auto response = +client(weblib::method::get, weblib::http(TEST_PORT), "/stop");
        BOOST_TEST(response.code == 200);
        BOOST_TEST(response.body == "Stopped");
    }
}

using G = cg::Graph<weblib::test::Client<0>, cg::Connect<weblib::Server, weblib::SimpleHandler>>;

BOOST_AUTO_TEST_CASE(Test)
{
    test_graph<G>(
        cg::Args<weblib::Server>("127.0.0.1", TEST_PORT, 1, false),
        cg::Args<weblib::SimpleHandler>(router),
        cg::Args<weblib::test::Client<0>>(client)
    );
}

BOOST_AUTO_TEST_SUITE_END()
