#include <weblib/server.hpp>
#include <weblib/handler.hpp>
#include <tester/metacg.hpp>
#include "lib/client.hpp"

#include <cg/host.hpp>
#include <cg/service.hpp>

BOOST_FIXTURE_TEST_SUITE(Proxy, tester::Metacg)

void router(crow::SimpleApp &app)
{
    CROW_ROUTE(app, "/test")([]{
        crow::response response(crow::status::OK);
        response.set_header("Content-Type", "application/json");
        response.set_header("Cache-Control", "max-age=2592000, Private");
        response.set_header("Expires", "Thu, 31 Dec 2037 23:55:55 GMT");
        return response;
    });

    CROW_ROUTE(app, "/stop")([&app]{
        app.stop();
        return "Stopped";
    });
}

void client(weblib::Client &client)
{
    {
        auto response = client(weblib::method::get, weblib::http(TEST_PORT), "/test").as<crow::response>();
        BOOST_TEST(response.code == 200);
        BOOST_TEST(response.get_header_value("Content-Type") == "application/json");
        BOOST_TEST(response.get_header_value("Cache-Control") == "max-age=2592000, Private");
        BOOST_TEST(response.get_header_value("Expires") == "Thu, 31 Dec 2037 23:55:55 GMT");
    }
    {
        auto response = +client(weblib::method::get, weblib::http(TEST_PORT), "/stop");
        BOOST_TEST(response.code == 200);
        BOOST_TEST(response.body == "Stopped");
    }
}

using G = cg::Graph<weblib::test::Client<0>, cg::Connect<weblib::Server, weblib::SimpleHandler>>;

BOOST_AUTO_TEST_CASE(Test)
{
    test_graph<G>(
        cg::Args<weblib::Server>("127.0.0.1", TEST_PORT, 1, false),
        cg::Args<weblib::SimpleHandler>(router),
        cg::Args<weblib::test::Client<0>>(client)
    );
}

BOOST_AUTO_TEST_SUITE_END()
