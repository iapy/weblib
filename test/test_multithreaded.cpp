#include <weblib/server.hpp>
#include <weblib/handler.hpp>
#include <tester/metacg.hpp>
#include "lib/client.hpp"

#include <cg/bind.hpp>
#include <cg/host.hpp>
#include <cg/service.hpp>

BOOST_FIXTURE_TEST_SUITE(Multithreaded, tester::Metacg)

std::atomic<int> responses{0};
std::atomic<int> value{0};
std::size_t finished{0};

std::condition_variable cv;
std::mutex mu;

struct Handler : cg::Component
{
    struct Impl
    {
        template<typename Base>
        struct Interface : cg::Bind<Interface, Base>
        {
            void setup(crow::SimpleApp &app)
            {
                CROW_ROUTE(app, "/stop")([&app]{
                    app.stop();
                    return "Stopped";
                });
                CROW_ROUTE(app, "/get")([]{
                    return std::to_string(++value);
                });
            }
        };
    };

    using Ports = ct::map<ct::pair<weblib::tag::Handler, Impl>>;
};

template<std::size_t I>
void client(weblib::Client &client)
{
    for(std::size_t i = 0; i < 10; ++i)
    {
        auto response = +client(weblib::method::get, weblib::http(TEST_PORT), "/get");
        BOOST_TEST(response.code == 200);
        ++responses;
    }

    if constexpr (0 == I)
    {
        std::unique_lock lock(mu);
        cv.wait(lock, [&]{
            return 4 == finished;
        });

        auto response = +client(weblib::method::get, weblib::http(TEST_PORT), "/stop");
        BOOST_TEST(response.code == 200);
        BOOST_TEST(responses == 50);
        BOOST_TEST(value == 50);
    }
    else
    {
        std::unique_lock lock(mu);
        ++finished;
        cv.notify_all();
    }
}

using G = cg::Graph<
    weblib::test::Client<0>,
    weblib::test::Client<1>,
    weblib::test::Client<2>,
    weblib::test::Client<3>,
    weblib::test::Client<4>,
    cg::Connect<weblib::Server, cg::Group<
        Handler
    >>
>;

BOOST_AUTO_TEST_CASE(Test)
{
    test_graph<G>(
        cg::Args<weblib::Server>("127.0.0.1", TEST_PORT, 5, false),
        cg::Args<weblib::test::Client<0>>(client<0>),
        cg::Args<weblib::test::Client<1>>(client<1>),
        cg::Args<weblib::test::Client<2>>(client<2>),
        cg::Args<weblib::test::Client<3>>(client<3>),
        cg::Args<weblib::test::Client<4>>(client<4>)
    );
}

BOOST_AUTO_TEST_SUITE_END()
