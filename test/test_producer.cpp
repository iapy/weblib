
#include <weblib/server.hpp>
#include <weblib/handler.hpp>
#include <tester/metacg.hpp>
#include "lib/client.hpp"

#include <cg/host.hpp>
#include <cg/service.hpp>

BOOST_FIXTURE_TEST_SUITE(Producer, tester::Metacg)

constexpr char DATA[] = {"streaming crowl\n"};
constexpr std::size_t COUNT = 200;
static std::size_t instances = 0;

struct Guard
{
    Guard() { ++instances; }
    ~Guard() { --instances; }

    Guard(Guard &&) { ++instances; }
    Guard(Guard const &) { ++instances; }
};

void router(crow::SimpleApp &app)
{
    CROW_ROUTE(app, "/")([]{
        crow::response response;

        response.set_header("Content-Length", std::to_string((sizeof(DATA) - 1) * COUNT));
        response.set_producer([count=std::size_t(0), guard=Guard()](net::asio::mutable_buffer &buf, boost::system::error_code &ec) mutable -> std::size_t {
            BOOST_TEST(1 == instances);
            if(++count == COUNT)
                ec = boost::asio::error::eof;
            std::memcpy(buf.data(), DATA, sizeof(DATA) - 1);
            return sizeof(DATA) - 1;
        });

        return response;
    });

    CROW_ROUTE(app, "/stop")([&app]{
        app.stop();
        return "Stopped";
    });
}

void client(weblib::Client &client)
{
    {
        auto response = client(weblib::method::get, weblib::http(TEST_PORT), "/").as<crow::response>();
        BOOST_TEST(response.code == 200);

        std::ostringstream ss;
        for(std::size_t i = 0; i < COUNT; ++i)
            ss.write(DATA, sizeof(DATA) - 1);

        BOOST_TEST(response.body == ss.str());
        BOOST_TEST(response.get_header_value("Content-Length") == std::to_string(ss.str().size()));
    }
    {
        auto response = +client(weblib::method::get, weblib::http(TEST_PORT), "/stop");
        BOOST_TEST(response.code == 200);
        BOOST_TEST(response.body == "Stopped");
    }
    BOOST_TEST(0 == instances);
}

using G = cg::Graph<weblib::test::Client<0>, cg::Connect<weblib::Server, weblib::SimpleHandler>>;

BOOST_AUTO_TEST_CASE(Test)
{
    test_graph<G>(
        cg::Args<weblib::Server>("127.0.0.1", TEST_PORT, 1, false),
        cg::Args<weblib::SimpleHandler>(router),
        cg::Args<weblib::test::Client<0>>(client)
    );
}

BOOST_AUTO_TEST_SUITE_END()
