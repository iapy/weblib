#include <weblib/server.hpp>
#include <weblib/handler.hpp>
#include <tester/metacg.hpp>
#include "lib/client.hpp"

#include <cg/host.hpp>
#include <cg/service.hpp>

BOOST_FIXTURE_TEST_SUITE(Simple, tester::Metacg)

void router(crow::SimpleApp &app)
{
    CROW_ROUTE(app, "/")([]{
        return __FILE__;
    });

    CROW_ROUTE(app, "/post").methods("POST"_method)([](crow::request const &request){
        return request.body;
    });

    CROW_ROUTE(app, "/put").methods("PUT"_method)([](crow::request const &request){
        return request.body + "&put=true";
    });

    CROW_ROUTE(app, "/del").methods("DELETE"_method)([](crow::request const &request){
        return request.body + "&delete=true";
    });

    CROW_ROUTE(app, "/stop")([&app]{
        app.stop();
        return "Stopped";
    });
}

void client(weblib::Client &client)
{
    {
        auto response = +client(weblib::method::get, weblib::http(TEST_PORT), "/");
        BOOST_TEST(response.code == 200);
        BOOST_TEST(response.body == __FILE__);
    }
    {
        auto response = +client(weblib::method::post, weblib::http(TEST_PORT), "/post", weblib::Form{{"foo", "bar"}});
        BOOST_TEST(response.code == 200);
        BOOST_TEST(response.body == "foo=bar");
    }
    {
        auto response = +client(weblib::method::put, weblib::http(TEST_PORT), "/put", weblib::Form{{"foo", "bar"}});
        BOOST_TEST(response.code == 200);
        BOOST_TEST(response.body == "foo=bar&put=true");
    }
    {
        auto response = +client(weblib::method::del, weblib::http(TEST_PORT), "/del", weblib::Form{{"foo", "bar"}});
        BOOST_TEST(response.code == 200);
        BOOST_TEST(response.body == "foo=bar&delete=true");
    }
    {
        auto response = +client(weblib::method::get, weblib::http(TEST_PORT), "/stop");
        BOOST_TEST(response.code == 200);
        BOOST_TEST(response.body == "Stopped");
    }
}

using G = cg::Graph<weblib::test::Client<0>, cg::Connect<weblib::Server, weblib::SimpleHandler>>;

BOOST_AUTO_TEST_CASE(Test)
{
    test_graph<G>(
        cg::Args<weblib::Server>("127.0.0.1", TEST_PORT, 1, false),
        cg::Args<weblib::SimpleHandler>(router),
        cg::Args<weblib::test::Client<0>>(client)
    );
}

BOOST_AUTO_TEST_SUITE_END()

