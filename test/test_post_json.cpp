#include <weblib/server.hpp>
#include <weblib/handler.hpp>
#include <tester/metacg.hpp>
#include "lib/client.hpp"

#include <cg/bind.hpp>
#include <cg/host.hpp>
#include <cg/service.hpp>

BOOST_FIXTURE_TEST_SUITE(Json, tester::Metacg)

void router(crow::SimpleApp &app)
{
    CROW_ROUTE(app, "/stop")([&app]{
        app.stop();
        return "Stopped";
    });
}

struct Handler : cg::Component
{
    struct Impl
    {
        template<typename Base>
        struct Interface : cg::Bind<Interface, Base>
        {
            void setup(crow::SimpleApp &app)
            {
                using Self = Interface<Base>;
                CROW_ROUTE(app, "/save").methods("POST"_method)(this->bind(&Self::save));
                CROW_ROUTE(app, "/load")(this->bind(&Self::load));
            }
        private:
            std::string save(crow::request const &value)
            {
                this->state = value.body;
                return load();
            }

            std::string load()
            {
                return this->state;
            }
        };
    };

    template<typename>
    using State = std::string;

    using Ports = ct::map<ct::pair<weblib::tag::Handler, Impl>>;
};

JSON_STRUCT(Foo, (
    (int, foo)
));

void client(weblib::Client &client)
{
    {
        auto response = +client(weblib::method::post, weblib::http(TEST_PORT), "/save", jsonio::to(Foo{.foo = 123}));
        BOOST_TEST(response.code == 200);
        BOOST_TEST(response.body == "{\"foo\":123}");
        BOOST_TEST(response.json<Foo>().foo == 123);
    }
    {
        auto response = +client( weblib::method::get, weblib::http(TEST_PORT),"/load");
        BOOST_TEST(response.code == 200);
        BOOST_TEST(response.body == "{\"foo\":123}");
        BOOST_TEST(response.json<Foo>().foo == 123);
    }
    {
        auto response = +client(weblib::method::get, weblib::http(TEST_PORT), "/stop");
        BOOST_TEST(response.code == 200);
        BOOST_TEST(response.body == "Stopped");
    }
}

using G = cg::Graph<weblib::test::Client<0>, cg::Connect<weblib::Server, cg::Group<
    weblib::SimpleHandler,
    Handler
>>>;

BOOST_AUTO_TEST_CASE(Test)
{
    test_graph<G>(
        cg::Args<weblib::Server>("127.0.0.1", TEST_PORT, 1, false),
        cg::Args<weblib::SimpleHandler>(router),
        cg::Args<weblib::test::Client<0>>(client)
    );
}

BOOST_AUTO_TEST_SUITE_END()
