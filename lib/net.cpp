#include <weblib/net.hpp>

namespace weblib {

std::string header(net::http::field field)
{
    const auto value = net::http::to_string(field);
    return std::string(std::begin(value), std::end(value));
}

} // namespace weblib
