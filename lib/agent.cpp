#include <weblib/agent.hpp>

#include <boost/algorithm/string/predicate.hpp>
#include <iostream>
#include <fstream>
#include <regex>

namespace weblib {

Agent::Agent()
: agent{WEBLIB_USER_AGENT}
{
}

bool Agent::tor(std::filesystem::path const &config)
{
    std::ifstream stream(config);
    std::regex const socks{"^SocksPort *(((25[0-5]|(2[0-4]|1\\d|[1-9]|)\\d)\\.?\\b){4}):(\\d*)"};

    for(std::string line; std::getline(stream, line);)
    {
        std::smatch matches;
        if(std::regex_search(line, matches, socks))
        {
            proxy.emplace(net::asio::ip::address::from_string(matches[1]), std::stoi(matches[5]));
            return true;
        }
    }
    return false;
}

void Agent::tor(net::tcp::endpoint &&p)
{
    proxy.emplace(std::forward<net::tcp::endpoint>(p));
}

std::optional<Agent::URL> Agent::parse(std::string const &url)
{
    static const char path = '/';

    URL result;

    char const *p = url.c_str();
    if(char const *q = std::strchr(p, '/'); NULL == q || (q - p) + 2 > url.size())
    {
        return std::nullopt;
    }
    else if(!std::strncmp(p, "https://", (q - p) + 2))
    {
        std::get<0>(result) = true;
        p = q + 2;
    }
    else if(!std::strncmp(p, "http://", (q - p) + 2))
    {
        std::get<0>(result) = false;
        p = q + 2;
    }
    else
    {
        return std::nullopt;
    }

    char const *q = p;
    while(*p && *p != ':' && *p != '?' && *p != '/')
        ++p;

    if((std::get<1>(result) = std::string_view(q, p - q)).empty())
    {
        return std::nullopt;
    }

    if(*p == ':')
    {
        for(++p; p && *p >= '0' && *p <= '9'; ++p)
            std::get<2>(result) = 10 * std::get<2>(result) + (*p - '0');
    }
    else
    {
        std::get<2>(result) = (std::get<0>(result) ? https.port : http.port);
    }

    std::get<3>(result) = (*p == 0)
        ? std::string_view(&path, 1)
        : std::string_view(p, url.size() - (p - url.c_str()));

    return result;
}

void Agent::socks(net::tcp::socket &socket, const char *host, std::uint16_t port)
{
    constexpr std::array<char, 3> NEGOTIATE{0x05, 0x01, 0x00};
    socket.send(net::asio::buffer(NEGOTIATE, NEGOTIATE.size()));

    std::array<char, 2> negotiate;
    socket.receive(net::asio::buffer(negotiate, negotiate.size()));
    bool connected = false;
    if(negotiate[0] == 5 && negotiate[1] == 0)
    {
        constexpr std::array<char, 4> CONNECT{0x05, 0x01, 0x00, 0x03};
        socket.send(net::asio::buffer(CONNECT, CONNECT.size()));

        std::uint8_t s = static_cast<std::uint8_t>(std::strlen(host));
        socket.send(net::asio::buffer(reinterpret_cast<char*>(&s), sizeof(s)));
        socket.send(net::asio::buffer(host, s));

        port = htons(port);
        socket.send(net::asio::buffer(reinterpret_cast<char*>(&port), sizeof(port)));

        std::array<char, 4> connect;
        socket.receive(net::asio::buffer(connect, connect.size()));

        if(connect[0] == 5 && connect[1] == 0 && connect[3] == 1) // IPv4 address
        {
            socket.receive(net::asio::buffer(connect, connect.size()));
            socket.receive(net::asio::buffer(reinterpret_cast<char*>(&port), sizeof(port)));
            if(decltype(connect){0,0,0,0} == connect && 0 == port)
            {
                connected = true;
            }
        }
    }
    if(!connected)
    {
        std::runtime_error ex("Socks proxy error");
        net::asio::detail::throw_exception(ex);
    }
}

Client::Raw Agent::socks(bool https, Client &&c, std::uint16_t port, Client::Request &&r) const
{
    if(https)
    {
        net::ssl::stream<net::tcp::socket> stream{c.context.ioc, c.context.sslctx};
        if(!SSL_set_tlsext_host_name(stream.native_handle(), c.host))
        {
            boost::system::error_code ec{static_cast<int>(::ERR_get_error()), boost::asio::error::get_ssl_category()};
            throw boost::system::system_error{ec};
        }
        stream.next_layer().connect(*proxy);
        socks(stream.next_layer(), c.host, port);
        stream.handshake(net::ssl::stream_base::client);
        return c.process(std::move(stream), std::move(r));
    }
    else
    {
        net::beast::tcp_stream stream(c.context.ioc);
        stream.connect(*proxy);
        socks(stream.socket(), c.host, port);
        return c.process(std::move(stream), std::move(r));
    }
}

Client::Raw Agent::handle(net::http::verb verb, std::optional<Client::Headers> const &h, std::string &&url, Agent::Payload &&data) const
{
    auto u = parse(url);
    if(!u)
    {
        std::runtime_error ex("Invalid URL");
        net::asio::detail::throw_exception(ex);
    }

    auto [https, host, port, path] = std::move(*u);

    auto c = Client(host.c_str(), agent.c_str());
    auto r = c.prepare(verb, path[0] == '/' ? std::string{path} : "/" + std::string{path});

    if(h)
    {
        for(auto const &[name, value] : *h) r.set(name, value);
    }
    if(data)
    {
        (*data)(r);
        r.prepare_payload();
    }

    if(boost::algorithm::ends_with(host, ".onion"))
    {
        if(!proxy)
        {
            std::runtime_error ex("Tor proxy not configured");
            net::asio::detail::throw_exception(ex);
        }
        else
        {
            return socks(https, std::move(c), port, std::move(r));
        }
    }
    else if(https)
    {
        return c.process(c.https(port), std::move(r));
    }
    else
    {
        return c.process(c.http(port), std::move(r));
    }
}

} // namespace weblib
