#include <weblib/async/client.hpp>
#include <weblib/client.hpp>
#include <sstream>
#include <vector>

namespace weblib {

std::size_t Client::HLIMIT = 8192;
thread_local Client::Context Client::context;

void Client::Raw::flatten()
{
    if(reader)
    {
        char buf[16384];
        std::size_t count;
        boost::asio::mutable_buffer m(buf, sizeof(buf));

        std::ostringstream body;
        boost::system::error_code ec;
        do
        {
            count = reader(m, ec);
            body.write(buf, count);
        }
        while(!ec);
        this->body() = std::move(body).str();
        crow::response::Producer{}.swap(reader);
    }
}

template<>
Client::Response Client::Raw::as<Client::Response>() &&
{
    if(reader)
    {
        char buf[16384];
        std::size_t count;
        boost::asio::mutable_buffer m(buf, sizeof(buf));

        std::ostringstream body;
        boost::system::error_code ec;
        do
        {
            count = reader(m, ec);
            body.write(buf, count);
        }
        while(!ec);
        return Client::Response{static_cast<int>(result_int()), std::move(body).str()};
    }
    else
    {
        return Client::Response{static_cast<int>(result_int()), body()};
    }
}

template<>
crow::response Client::Raw::as<crow::response>() &&
{
    crow::response response{static_cast<int>(result_int())};
    for(auto it = base().begin(); it != base().end(); ++it)
    {
        auto const n = net::http::to_string(it->name());
        auto const v = it->value();

        response.set_header(
            std::string(std::begin(n), std::end(n)),
            std::string(std::begin(v), std::end(v))
        );
    }
    if(reader)
    {
        response.set_producer(std::move(reader));
    }
    else
    {
        response.body = body();
    }
    return response;
}

Client::Response Client::Raw::operator + () &&
{
    return std::move(*this).as<Client::Response>();
}

Client::Request Client::prepare(net::http::verb verb, std::string const &path) const
{
    using namespace net;
    Request req{verb, path, 11};
    req.set(http::field::host, host);
    req.set(http::field::user_agent, agent);
    std::visit([&req](auto const *key){
        weblib::cookie::load(key, req);
    }, cookie);
    headers_(req, verb, path);
    return req;
}

net::beast::tcp_stream Client::http(std::uint16_t port) const
{
    auto const results = context.resolver.resolve(host, std::to_string(port));
    net::beast::tcp_stream stream(context.ioc);
    stream.connect(results);
    return stream;
}

net::ssl::stream<net::tcp::socket> Client::https(std::uint16_t port) const
{
    auto const results = context.resolver.resolve(host, std::to_string(port));
    net::ssl::stream<net::tcp::socket> stream{context.ioc, context.sslctx};
    if(!SSL_set_tlsext_host_name(stream.native_handle(), host))
    {
        boost::system::error_code ec{static_cast<int>(::ERR_get_error()), boost::asio::error::get_ssl_category()};
        throw boost::system::system_error{ec};
    }
    net::asio::connect(stream.next_layer(), results.begin(), results.end());
    stream.handshake(net::ssl::stream_base::client);
    return stream;
}

template<>
void Client::payload(Client::Request &request, std::string &&str)
{
    request.set(net::http::field::content_type, "text/plain");
    request.body() = std::move(str);
}

template<>
void Client::payload(Client::Request &request, nlohmann::json &&json)
{
    request.set(net::http::field::content_type, "application/json");
    request.body() = json.dump();
}

template<>
void Client::payload(Client::Request &request, std::vector<char> &&binary)
{
    request.set(net::http::field::content_type, "application/x-binary");
    request.body() = std::string(std::begin(binary), std::end(binary));
}

template<>
void Client::payload(Client::Request &request, Form &&form)
{
    request.set(net::http::field::content_type, "application/x-www-form-urlencoded");
    request.body() = [&form]{
        std::ostringstream ss;
        for(auto it = form.begin(); it != form.end(); ++it)
        {
            if(it != form.begin()) ss << '&';
            ss << it->first << '=' << it->second;
        }
        return ss.str();
    }();
}

template<typename Stream>
void Client::read(Stream&& stream, Client::Raw &response, net::beast::error_code &ec)
{
    using Parser = net::http::parser<Raw::is_request::value, Raw::body_type, Raw::allocator_type>;
    Parser parser(std::move(response));

    parser.eager(true);
    parser.header_limit(HLIMIT);

    net::beast::flat_buffer buffer;
    if(net::http::read(stream, buffer, parser, ec); ec)
    {
        if(ec == net::http::error::body_limit && parser.is_header_done())
        {
            response = parser.release();
            response.reader = [stream=std::make_shared<Stream>(std::move(stream)), body=std::optional<net::beast::flat_buffer>(std::move(buffer))]
                (boost::asio::mutable_buffer &buffer, boost::system::error_code &ec) mutable -> std::size_t {
                    if(body)
                    {
                        std::size_t n = std::min(buffer.size(), body->size());
                        std::memcpy(buffer.data(), boost::asio::buffer_cast<char const *>(body->cdata()), n);

                        if(body->consume(n); !body->size())
                        {
                            body.reset();
                        }
                        return n;
                    }
                    return net::asio::read(*stream, buffer, ec);
                };
        }
        else
        {
            response.result(504);
        }
    }
    else
    {
        response = parser.release();
        std::visit([&response](auto const *key){
            weblib::cookie::save(key, response);
        }, cookie);
    }
}

template<typename Stream>
Client::Raw Client::process(Stream &&stream, Client::Request &&request)
{
    Raw response;
    net::beast::error_code ec;
    if(net::http::write(stream, request, ec); ec)
    {
        response.result(504);
    }
    else
    {
        read(std::forward<Stream>(stream), response, ec);
    }
    return response;
}

template Client::Raw Client::process<net::beast::tcp_stream>(net::beast::tcp_stream &&, Client::Request &&);
template Client::Raw Client::process<net::ssl::stream<net::tcp::socket>>(net::ssl::stream<net::tcp::socket> &&, Client::Request &&);

template<typename Stream>
Client::Uploader Client::uploader(Stream &&stream, Request &&request, std::size_t length)
{
    net::beast::error_code ec;
    if(net::http::write(stream, request, ec); ec)
    {
        return [response=std::make_shared<Raw>()](const char *, std::size_t) mutable -> std::shared_ptr<Raw> {
            response->result(504);
            return response;
        };
    }
    else
    {
        return [response=std::make_shared<Raw>(), stream=std::make_shared<Stream>(std::forward<Stream>(stream)), length, this](const char *data, std::size_t size) mutable -> std::shared_ptr<Raw> {
            net::beast::error_code ec;
            boost::asio::const_buffer buf(data, size);

            if(boost::asio::write(*stream, buf, ec); ec)
            {
                length = 0;
                response->result(504);
            }
            else
            {
                response->result(100);
            }

            if(length <= size)
            {
                read(std::forward<Stream>(*stream), *response, ec);
            }
            else
            {
                length -= size;
            }
            return response;
        };
    }
}

template Client::Uploader Client::uploader<net::beast::tcp_stream>(net::beast::tcp_stream &&, Client::Request &&, std::size_t);
template Client::Uploader Client::uploader<net::ssl::stream<net::tcp::socket>>(net::ssl::stream<net::tcp::socket> &&, Client::Request &&, std::size_t);

template<typename Stream>
Client::Raw Client::forward(Stream &&stream, crow::request const &request)
{
    net::http::verb verb;
    switch(request.method)
    {
        case crow::HTTPMethod::Delete:
            verb = net::http::verb::delete_; break;
        case crow::HTTPMethod::Get:
            verb = net::http::verb::get; break;
        case crow::HTTPMethod::Head:
            verb = net::http::verb::head; break;
        case crow::HTTPMethod::Post:
            verb = net::http::verb::post; break;
        case crow::HTTPMethod::Put:
            verb = net::http::verb::put; break;
        case crow::HTTPMethod::Connect:
            verb = net::http::verb::connect; break;
        case crow::HTTPMethod::Options:
            verb = net::http::verb::options; break;
        case crow::HTTPMethod::Trace:
            verb = net::http::verb::trace; break;
        case crow::HTTPMethod::Patch:
            verb = net::http::verb::patch; break;
        case crow::HTTPMethod::Purge:
            verb = net::http::verb::purge; break;
        case crow::HTTPMethod::Copy:
            verb = net::http::verb::copy; break;
        case crow::HTTPMethod::Lock:
            verb = net::http::verb::lock; break;
        case crow::HTTPMethod::MkCol:
            verb = net::http::verb::mkcol; break;
        case crow::HTTPMethod::Move:
            verb = net::http::verb::move; break;
        case crow::HTTPMethod::Propfind:
            verb = net::http::verb::propfind; break;
        case crow::HTTPMethod::Proppatch:
            verb = net::http::verb::proppatch; break;
        case crow::HTTPMethod::Search:
            verb = net::http::verb::search; break;
        case crow::HTTPMethod::Unlock:
            verb = net::http::verb::unlock; break;
        case crow::HTTPMethod::Bind:
            verb = net::http::verb::bind; break;
        case crow::HTTPMethod::Rebind:
            verb = net::http::verb::rebind; break;
        case crow::HTTPMethod::Unbind:
            verb = net::http::verb::unbind; break;
        case crow::HTTPMethod::Acl:
            verb = net::http::verb::acl; break;
        case crow::HTTPMethod::Report:
            verb = net::http::verb::report; break;
        case crow::HTTPMethod::MkActivity:
            verb = net::http::verb::mkactivity; break;
        case crow::HTTPMethod::Checkout:
            verb = net::http::verb::checkout; break;
        case crow::HTTPMethod::Merge:
            verb = net::http::verb::merge; break;
        case crow::HTTPMethod::MSearch:
            verb = net::http::verb::msearch; break;
        case crow::HTTPMethod::Notify:
            verb = net::http::verb::notify; break;
        case crow::HTTPMethod::Subscribe:
            verb = net::http::verb::subscribe; break;
        case crow::HTTPMethod::Unsubscribe:
            verb = net::http::verb::unsubscribe; break;
        case crow::HTTPMethod::MkCalendar:
            verb = net::http::verb::mkcalendar; break;
        case crow::HTTPMethod::Link:
            verb = net::http::verb::link; break;
        case crow::HTTPMethod::Unlink:
            verb = net::http::verb::unlink; break;
    }

    using namespace net;
    Request req{verb, request.raw_url, 11};
    for(auto &[k, v]: request.headers)
    {
        req.set(net::http::string_to_field(k), v);
    }
    headers_(req, verb, request.raw_url);

    Raw response;
    net::beast::error_code ec;
    if(net::http::write(stream, req, ec); ec)
    {
        response.result(504);
    }
    else
    {
        for(std::size_t b = 0; b < request.body.size();)
        {
            std::size_t e = std::min(b + 16384, request.body.size());
            boost::asio::const_buffer buf(request.body.c_str() + b, e - b);
            if(boost::asio::write(stream, buf, ec); ec)
            {
                response.result(504);
            }
            b = e;
        }
        if(net::http::status::gateway_timeout != response.result())
        {
            read(std::forward<Stream>(stream), response, ec);
        }
    }
    return response;
}

template Client::Raw Client::forward<net::beast::tcp_stream>(net::beast::tcp_stream &&, crow::request const &);
template Client::Raw Client::forward<net::ssl::stream<net::tcp::socket>>(net::ssl::stream<net::tcp::socket> &&, crow::request const &);

Client::~Client()
{
    if(std::holds_alternative<Client const *>(cookie))
        weblib::cookie::free(this);
}

} // namespace weblib

namespace weblib::async {

Client::Client(net::beast::tcp_stream &&stream)
: stream_(std::forward<net::beast::tcp_stream>(stream))
, connected_(true)
{
}

Client::Client(Client::Executor const &ex, net::tcp::resolver::results_type const &results)
: stream_(ex)
{
    net::beast::error_code ec;
    stream_.connect(results, ec);
    connected_ = !static_cast<bool>(ec);
}

} // namespace weblib::async
