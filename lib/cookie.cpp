#include <weblib/cookie.hpp>
#include <unordered_map>
#include <shared_mutex>

namespace weblib::cookie {
namespace {

std::unordered_map<std::string, std::string> by_host;
std::unordered_map<Client const *, std::string> by_inst;

std::shared_mutex by_host_mu;
std::shared_mutex by_inst_mu;

} // namespace

void load(Client const *key, Request &req)
{
    std::shared_lock<std::shared_mutex> lock(by_inst_mu);
    if(auto it = by_inst.find(key); it != by_inst.end() && !it->second.empty())
    {
        req.set(net::http::field::cookie, it->second);
    }
}

void load(char const *key, Request &req)
{
    std::shared_lock<std::shared_mutex> lock(by_host_mu);
    if(auto it = by_host.find(key); it != by_host.end() && !it->second.empty())
    {
        req.set(net::http::field::cookie, it->second);
    }
}

void save(Client const *key, Response const &res)
{
    if(auto it = res.base().find(net::http::field::set_cookie); it != res.base().end())
    {
        std::unique_lock<std::shared_mutex> lock(by_inst_mu);
        by_inst[key] = std::string{it->value()};
    }
}

void save(char const *key, Response const &res)
{
    if(auto it = res.base().find(net::http::field::set_cookie); it != res.base().end())
    {
        std::unique_lock<std::shared_mutex> lock(by_host_mu);
        by_host[key] = std::string{it->value()};
    }
}

void free(Client const *key)
{
    std::unique_lock<std::shared_mutex> lock(by_inst_mu);
    by_inst.erase(key);
}

} // namespace weblib::cookie 
