#include <weblib/server.hpp>
#include <mutex>

namespace {

std::mutex guard;
std::unordered_map<std::string, std::unique_ptr<std::string>> files;

} // namespace

const char * weblib::Server::StateBase::file(std::string &&name) const
{
    std::unique_lock<std::mutex> lock(guard);
    auto &filename = files[name];
    if(!filename)
    {
        filename = std::make_unique<std::string>(std::move(name));
    }
    return filename->c_str();
}
