#pragma once
#include <sstream>

#define WEBLIB_RENDER(fn) [&](auto&&... args) { std::ostringstream ss; fn(ss, std::forward<decltype(args)>(args)...); return ss.str(); }
