#pragma once

namespace weblib {
namespace tag {

    struct Handler {};
    struct Headers {};

    namespace async
    {
        struct Handler {};
        struct Chunker {};
        struct Context {};
    }

} // namespace tag
} // namespace weblib
