#pragma once
#include <jsonio/serialize.hpp>
#include <crow.h>

namespace weblib {

template<typename T>
inline crow::response response(crow::status status, T &&value)
{
    return crow::response(status, "json", jsonio::to(value).dump());
}

template<typename T>
inline crow::response response(T &&value)
{
    return response(crow::status::OK, std::forward<T>(value));
}

} // namespace weblib
