#pragma once

#include <weblib/tags.hpp>
#include <cg/component.hpp>

#include <crow.h>
#include <functional>

namespace weblib {

struct SimpleHandler : cg::Component
{
    struct Impl
    {
        template<typename Base>
        struct Interface : Base
        {
            template<typename ...Middleware>
            void setup(crow::App<Middleware...> &app)
            {
                std::move(this->state.handler)(app);
            }
        };
    };

    template<typename>
    struct State
    {
        std::function<void(crow::SimpleApp&)> handler;
        State(decltype(handler) &&handler) : handler{handler} {}
    };

    using Ports = ct::map<ct::pair<tag::Handler, Impl>>;
};

} // namespace weblib
