#pragma once
#include <weblib/cookie.hpp>

#include <jsonio/deserialize.hpp>
#include <crow.h>

#include <functional>
#include <iomanip>
#include <iostream>
#include <optional>
#include <unordered_map>

#ifndef WEBLIB_USER_AGENT
#define WEBLIB_USER_AGENT BOOST_BEAST_VERSION_STRING
#endif

namespace weblib {

template<typename I>
std::string escape(I b, I e)
{
    std::ostringstream escaped;
    escaped.fill('0');
    escaped << std::hex;

    for(I p = b; p != e; ++p)
    {
        if(isalnum(*p) || *p == '-' || *p == '_' || *p == '.' || *p == '~')
            escaped << *p;
        else
            escaped << std::uppercase << '%' << std::setw(2) << int((unsigned char) *p) << std::nouppercase;
    }
    return escaped.str();
}

template<typename I>
std::string unescape(I b, I e)
{
    char hex[3] = {0,0,0};
    std::ostringstream unescaped;
    for(I p = b; p != e; ++p)
    {
        if(*p == '%')
        {
            if(++p == e) return unescaped.str(); hex[0] = *p;
            if(++p == e) return unescaped.str(); hex[1] = *p;
            unescaped << static_cast<char>(std::stoul(&hex[0], nullptr, 16));
        }
        else
        {
            unescaped << *p;
        }
    }
    return unescaped.str();
}

constexpr Port<80> http;
constexpr Port<443> https;

class Agent;
class Client
{
    friend class Agent;
public:
    using Request = cookie::Request;
    using Headers = std::unordered_map<net::http::field, std::string>;

    static std::size_t HLIMIT;

    struct Response
    {
        int const code;
        std::string const body;

        template<typename T = void>
        auto json() const -> std::conditional_t<std::is_same_v<T, void>, nlohmann::json, T>
        {
            if constexpr (std::is_same_v<T, void>)
                return nlohmann::json::parse(body);
            else
                return jsonio::from<T>(nlohmann::json::parse(body));
        }
    };

    struct Raw : cookie::Response
    {
        inline Raw& operator = (cookie::Response &&response)
        {
            cookie::Response::operator = (std::forward<cookie::Response>(response));
            return *this;
        }

        template<typename T = Response>
        auto as() && -> std::enable_if_t<(std::is_same_v<T, Response> || std::is_same_v<T, crow::response>), T>;

        void flatten();
        Response operator + () &&;
        crow::response::Producer reader;
    };

    using Uploader = std::function<std::shared_ptr<Raw>(const char *, std::size_t)>;

    Client(Client const &) = delete;
    Client &operator = (Client const &) = delete;

    Client(Client &&) = default;
    Client &operator = (Client &&) = default;

    Client(const char *host)
    : host{host}, agent{WEBLIB_USER_AGENT}, cookie{this}
    {
    }

    Client(const char *host, const char *agent)
    : host{host}, agent{agent}, cookie{host}
    {
    }

    ~Client();

    template<typename Functor>
    void headers(Functor &&functor)
    {
        headers_ = decltype(headers_){std::move(functor)};
    }

    template<net::http::verb method, std::uint16_t port>
    auto operator () (std::integral_constant<net::http::verb, method>, Port<port> p, std::string const &path)
    -> std::enable_if_t<(method == method::get.value), Raw>
    {
        if constexpr (weblib::https == port)
        {
            return handle(https(p.port), method, path, std::nullopt);
        }
        else
        {
            return handle(http(p.port), method, path, std::nullopt);
        }
    }

    template<net::http::verb method, std::uint16_t port, typename Data>
    auto operator () (std::integral_constant<net::http::verb, method>, Port<port> p, std::string const &path, Data &&data)
    -> std::enable_if_t<(method != method::get.value), Raw>
    {
        if constexpr (weblib::https == port)
        {
            return handle(https(p.port), method, path, std::forward<Data>(data), std::nullopt);
        }
        else
        {
            return handle(http(p.port), method, path, std::forward<Data>(data), std::nullopt);
        }
    }

    template<net::http::verb method, std::uint16_t port>
    auto operator () (std::integral_constant<net::http::verb, method>, Port<port> p, std::string const &path, std::size_t length)
    -> std::enable_if_t<(method != method::get.value), Uploader>
    {
        if constexpr (weblib::https == port)
        {
            return uploader(https(p.port), method, path, length, std::nullopt);
        }
        else
        {
            return uploader(http(p.port), method, path, length, std::nullopt);
        }
    }

    template<net::http::verb method, std::uint16_t port>
    auto operator () (std::integral_constant<net::http::verb, method>, Port<port> p, Headers &&headers, std::string const &path)
    -> std::enable_if_t<(method == method::get.value), Raw>
    {
        if constexpr (weblib::https == port)
        {
            return handle(https(p.port), method, path, std::forward<Headers>(headers));
        }
        else
        {
            return handle(http(p.port), method, path, std::forward<Headers>(headers));
        }
    }

    template<net::http::verb method, std::uint16_t port>
    auto operator () (std::integral_constant<net::http::verb, method>, Port<port> p, Headers &&headers, std::string const &path, std::size_t length)
    -> std::enable_if_t<(method != method::get.value), Uploader>
    {
        if constexpr (weblib::https == port)
        {
            return uploader(https(p.port), method, path, length, std::forward<Headers>(headers));
        }
        else
        {
            return uploader(http(p.port), method, path, length, std::forward<Headers>(headers));
        }
    }

    template<net::http::verb method, std::uint16_t port, typename Data>
    auto operator () (std::integral_constant<net::http::verb, method>, Port<port> p, Headers &&headers, std::string const &path, Data &&data)
    -> std::enable_if_t<(method != method::get.value), Raw>
    {
        if constexpr (weblib::https == port)
        {
            return handle(https(p.port), method, path, std::forward<Data>(data), std::forward<Headers>(headers));
        }
        else
        {
            return handle(http(p.port), method, path, std::forward<Data>(data), std::forward<Headers>(headers));
        }
    }

    template<std::uint16_t port>
    Raw forward(Port<port> p, crow::request const &request)
    {
        if constexpr (weblib::https == port)
        {
            return forward(https(p.port), request);
        }
        else
        {
            return forward(http(p.port), request);
        }
    }

    template<typename Data>
    static void payload(Request &request, Data &&data);
private:
    net::beast::tcp_stream http(std::uint16_t port) const;
    net::ssl::stream<net::tcp::socket> https(std::uint16_t port) const;

    Request prepare(net::http::verb verb, std::string const &path) const;

    template<typename Stream>
    Raw handle(Stream &&stream, net::http::verb verb, std::string const &path, std::optional<Headers> &&h)
    {
        Request request = prepare(verb, path);
        if(h)
        {
            for(auto const &[name, value] : *h) request.set(name, value);
        }
        return process(std::forward<Stream>(stream), std::move(request));
    }

    template<typename Stream, typename Data>
    Raw handle(Stream &&stream, net::http::verb verb, std::string const &path, Data &&data, std::optional<Headers> &&h)
    {
        Request request = prepare(verb, path);
        if(h)
        {
            for(auto const &[name, value] : *h) request.set(name, value);
        }
        payload(request, std::forward<Data>(data));
        request.prepare_payload();
        return process(std::forward<Stream>(stream), std::move(request));
    }

    template<typename Stream>
    Uploader uploader(Stream &&stream, net::http::verb verb, std::string const &path, std::size_t length, std::optional<Headers> &&h)
    {
        Request request = prepare(verb, path);
        if(h)
        {
            for(auto const &[name, value] : *h) request.set(name, value);
        }
        request.content_length(length);
        return uploader(std::forward<Stream>(stream), std::move(request), length);
    }

    template<typename Stream>
    void read(Stream &&, Raw &, net::beast::error_code &);

    template<typename Stream>
    Raw process(Stream &&stream, Request &&request);

    template<typename Stream>
    Raw forward(Stream &&stream, crow::request const &request);

    template<typename Stream>
    Uploader uploader(Stream &&stream, Request &&request, std::size_t);

private:
    struct Context
    {
        net::asio::io_context ioc;
        net::tcp::resolver resolver{ioc};
        net::ssl::context sslctx{net::ssl::context::sslv23_client};
    };

    char const *host;
    char const *agent;
    std::variant<Client const *, char const *> const cookie;
    static thread_local Context context;

    std::function<void(Request&, net::http::verb verb, std::string const&)> headers_{+[](Request&, net::http::verb verb, std::string const&){
    }};
};

using Form = std::unordered_map<std::string, std::string>;

} // namespace weblib

