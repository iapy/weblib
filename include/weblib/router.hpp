#pragma once
#include <weblib/tags.hpp>
#include <cg/connect.hpp>

#include <boost/preprocessor.hpp>
#include <crow.h>

#define WEBLIB_ROUTE(app, url) app.template route<crow::black_magic::get_parameter_tag(url)>(url)
#define WEBLIB_DYNAMIC_ROUTE(app, url) app.route_dynamic(url)
#define WEBLIB_BP_DYNAMIC_ROUTE(url) this->state.blueprint.new_rule_dynamic(url)

#define WEBLIB_BP_ROUTE_1(url) this->state.blueprint.template new_rule_tagged<crow::black_magic::get_parameter_tag(url)>(url)
#define WEBLIB_BP_ROUTE_2(blueprint, url) this->state.blueprint.template new_rule_tagged<crow::black_magic::get_parameter_tag(url)>(url)

#define WEBLIB_BP_ROUTE(...) \
    BOOST_PP_OVERLOAD(WEBLIB_BP_ROUTE_, __VA_ARGS__)(__VA_ARGS__)

