#pragma once
#include <weblib/client.hpp>
#include <optional>
#include <utility>

namespace weblib {

class Agent 
{
public:
    Agent();
    Agent(Agent &&) = default;

    void set(std::string &&agent)
    {
        this->agent = agent;
    }

    void tor(net::tcp::endpoint &&);
    bool tor(std::filesystem::path const &);

    using URL = std::tuple
    <
        bool, std::string, std::uint16_t, std::string_view
    >;

    static std::optional<URL> parse(std::string const &url);

public:
    template<net::http::verb method>
    auto operator () (std::integral_constant<net::http::verb, method>, std::string &&url) const
    -> std::enable_if_t<(method == method::get.value), Client::Raw>
    {
        return handle(method, std::nullopt, std::forward<std::string>(url));
    }

    template<net::http::verb method, typename Data>
    auto operator () (std::integral_constant<net::http::verb, method>, std::string &&url, Data &&data) const
    -> std::enable_if_t<(method != method::get.value), Client::Raw>
    {
        using namespace std::placeholders;
        return handle(method, std::nullopt, std::forward<std::string>(url), [data=std::forward<Data>(data)](Client::Request &request) mutable {
            Client::payload<Data>(request, std::forward<Data>(data));
        });
    }

    template<net::http::verb method>
    auto operator () (std::integral_constant<net::http::verb, method>, Client::Headers &&headers, std::string &&url) const
    -> std::enable_if_t<(method == method::get.value), Client::Raw>
    {
        return handle(method, headers, std::forward<std::string>(url));
    }

    template<net::http::verb method, typename Data>
    auto operator () (std::integral_constant<net::http::verb, method>, Client::Headers &&headers, std::string &&url, Data &&data) const
    -> std::enable_if_t<(method != method::get.value), Client::Raw>
    {
        using namespace std::placeholders;
        return handle(method, headers, std::forward<std::string>(url), [data=std::forward<Data>(data)](Client::Request &request) mutable {
            Client::payload<Data>(request, std::forward<Data>(data));
        });
    }

private:
    using Payload = std::optional<std::function<void(Client::Request &)>>;
    Client::Raw handle(net::http::verb, std::optional<Client::Headers> const &, std::string &&, Payload &&data = std::nullopt) const;

    Client::Raw socks(bool, Client &&, std::uint16_t port, Client::Request &&) const;
    static void socks(net::tcp::socket &socket, const char *host, std::uint16_t port);

private:
    std::string agent;
    std::optional<net::tcp::endpoint> proxy;
};

} // namesapace weblib

namespace jsonio {

template<>
struct Serializer<weblib::Agent>
{
    template<typename Data = void*>
    static void load(nlohmann::json const &json, weblib::Agent &value, Data d = nullptr)
    {
        value.set(json.get<std::string>());
    }
};

} // namespace jsonio
