#pragma once
#include <weblib/net.hpp>

namespace weblib {

class Client;

namespace cookie {

using Request = net::http::request<net::http::string_body>;
using Response = net::http::response<net::http::string_body>;

void load(Client const *key, Request&);
void load(char const *key, Request&);

void save(Client const *key, Response const &);
void save(char const *key, Response const &);

void free(Client const *key);

} // namespace cookie
} // namespace weblib
