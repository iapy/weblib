#pragma once
#include <boost/beast/core.hpp>
#include <boost/beast/http.hpp>
#include <boost/beast/version.hpp>
#include <boost/asio/connect.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/asio/ssl/error.hpp>
#include <boost/asio/ssl/stream.hpp>
#include <boost/asio/streambuf.hpp>

namespace net
{
    namespace beast = boost::beast;
    namespace http  = boost::beast::http;
    namespace ssl   = boost::asio::ssl;
    namespace asio  = boost::asio;
    using tcp   = boost::asio::ip::tcp;
} // namespace net

namespace weblib
{

template<std::uint16_t port_>
struct Port
{
    std::uint16_t port = port_;

    Port operator () (std::uint16_t port) const
    {
        return Port{port};
    }

    constexpr bool operator == (std::uint16_t port) const
    {
        return port_ == port;
    }
};

std::string header(net::http::field field);

} // namespace weblib

namespace weblib::method
{
#define WEBLIB_DECLARE_METHOD(name) constexpr std::integral_constant<net::http::verb, net::http::verb::name> name;
    WEBLIB_DECLARE_METHOD(acl)
    WEBLIB_DECLARE_METHOD(bind)
    WEBLIB_DECLARE_METHOD(checkout)
    WEBLIB_DECLARE_METHOD(connect)
    WEBLIB_DECLARE_METHOD(copy)
    constexpr std::integral_constant<net::http::verb, net::http::verb::delete_> del;
    WEBLIB_DECLARE_METHOD(get)
    WEBLIB_DECLARE_METHOD(head)
    WEBLIB_DECLARE_METHOD(link)
    WEBLIB_DECLARE_METHOD(lock)
    WEBLIB_DECLARE_METHOD(merge)
    WEBLIB_DECLARE_METHOD(mkactivity)
    WEBLIB_DECLARE_METHOD(mkcalendar)
    WEBLIB_DECLARE_METHOD(mkcol)
    WEBLIB_DECLARE_METHOD(move)
    WEBLIB_DECLARE_METHOD(msearch)
    WEBLIB_DECLARE_METHOD(notify)
    WEBLIB_DECLARE_METHOD(options)
    WEBLIB_DECLARE_METHOD(patch)
    WEBLIB_DECLARE_METHOD(post)
    WEBLIB_DECLARE_METHOD(propfind)
    WEBLIB_DECLARE_METHOD(proppatch)
    WEBLIB_DECLARE_METHOD(purge)
    WEBLIB_DECLARE_METHOD(put)
    WEBLIB_DECLARE_METHOD(rebind)
    WEBLIB_DECLARE_METHOD(report)
    WEBLIB_DECLARE_METHOD(search)
    WEBLIB_DECLARE_METHOD(subscribe)
    WEBLIB_DECLARE_METHOD(trace)
    WEBLIB_DECLARE_METHOD(unbind)
    WEBLIB_DECLARE_METHOD(unlink)
    WEBLIB_DECLARE_METHOD(unlock)
#undef WEBLIB_DECLARE_METHOD
}

namespace weblib::async::method
{
#define WEBLIB_ASYNC_DECLARE_METHOD(name) using name = std::integral_constant<net::http::verb, net::http::verb::name>;
    WEBLIB_ASYNC_DECLARE_METHOD(acl)
    WEBLIB_ASYNC_DECLARE_METHOD(bind)
    WEBLIB_ASYNC_DECLARE_METHOD(checkout)
    WEBLIB_ASYNC_DECLARE_METHOD(connect)
    WEBLIB_ASYNC_DECLARE_METHOD(copy)
    WEBLIB_ASYNC_DECLARE_METHOD(delete_)
    WEBLIB_ASYNC_DECLARE_METHOD(get)
    WEBLIB_ASYNC_DECLARE_METHOD(head)
    WEBLIB_ASYNC_DECLARE_METHOD(link)
    WEBLIB_ASYNC_DECLARE_METHOD(lock)
    WEBLIB_ASYNC_DECLARE_METHOD(merge)
    WEBLIB_ASYNC_DECLARE_METHOD(mkactivity)
    WEBLIB_ASYNC_DECLARE_METHOD(mkcalendar)
    WEBLIB_ASYNC_DECLARE_METHOD(mkcol)
    WEBLIB_ASYNC_DECLARE_METHOD(move)
    WEBLIB_ASYNC_DECLARE_METHOD(msearch)
    WEBLIB_ASYNC_DECLARE_METHOD(notify)
    WEBLIB_ASYNC_DECLARE_METHOD(options)
    WEBLIB_ASYNC_DECLARE_METHOD(patch)
    WEBLIB_ASYNC_DECLARE_METHOD(post)
    WEBLIB_ASYNC_DECLARE_METHOD(propfind)
    WEBLIB_ASYNC_DECLARE_METHOD(proppatch)
    WEBLIB_ASYNC_DECLARE_METHOD(purge)
    WEBLIB_ASYNC_DECLARE_METHOD(put)
    WEBLIB_ASYNC_DECLARE_METHOD(rebind)
    WEBLIB_ASYNC_DECLARE_METHOD(report)
    WEBLIB_ASYNC_DECLARE_METHOD(search)
    WEBLIB_ASYNC_DECLARE_METHOD(subscribe)
    WEBLIB_ASYNC_DECLARE_METHOD(trace)
    WEBLIB_ASYNC_DECLARE_METHOD(unbind)
    WEBLIB_ASYNC_DECLARE_METHOD(unlink)
    WEBLIB_ASYNC_DECLARE_METHOD(unlock)
#undef WEBLIB_ASYNC_DECLARE_METHOD
};
