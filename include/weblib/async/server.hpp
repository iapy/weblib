#pragma once
#include <weblib/net.hpp>
#include <weblib/tags.hpp>
#include <msgbus/tags.hpp>
#include <logger/log.hpp>

#include <cg/component.hpp>
#include <cg/config.hpp>
#include <algorithm>
#include <memory>

#include <boost/beast/core/string_type.hpp>
#include <boost/asio/buffers_iterator.hpp>
#include <boost/asio/read.hpp>

#ifdef WEBLIB_LOG_INFO
#undef WEBLIB_LOG_INFO
#define WEBLIB_LOG_INFO(...) LOG_INFO(__VA_ARGS__)
#else
#define WEBLIB_LOG_INFO(...) LOG_DEBUG(__VA_ARGS__)
#endif

namespace jsonio {
template<typename Body>
struct Serializer<net::beast::http::response<Body>>
{
    template<typename Data = void*>
    static void save(nlohmann::json &json, net::beast::http::response<Body> const &value, Data d = nullptr)
    {
        json["code"] = value.result();
        for(auto const &header : value)
        {
            json["headers"][boost::beast::http::to_string(header.name())] = header.value();
        }
    }
};

template<>
struct Serializer<net::beast::http::request<net::beast::http::string_body>>
{
    template<typename Data = void*>
    static void save(nlohmann::json &json, net::beast::http::request<net::beast::http::string_body> const &value, Data d = nullptr)
    {
        json["path"] = value.target();
        json["method"] = value.method_string();
        for(auto const &header : value)
        {
            json["headers"][header.name_string()] = header.value();
        }
    }
};
} // jsonio

namespace weblib::async {
namespace detail {

template<typename Handler, typename Context, typename Method>
auto handler_supports_method(Handler *handler) -> decltype (
    handler->handle(Method{}, std::declval<Context>()), std::true_type{}
);

template<typename Handler, typename Context, typename Method>
std::false_type handler_supports_method(...);

template<typename Handler, typename Context, typename Method>
constexpr bool handler_supports_method_v = decltype(handler_supports_method<Handler, Context, Method>(nullptr))::value;

template<typename Handler, typename Context>
auto handler_supports_authentication(Handler *handler) -> decltype (
    handler->authenticate(std::declval<Context>()), std::true_type{}
);

template<typename Handler, typename Context>
std::false_type handler_supports_authentication(...);

template<typename Handler, typename Context>
constexpr bool handler_supports_authentication_v = decltype(handler_supports_authentication<Handler, Context>(nullptr))::value;

template<typename T>
auto context_type(T*) -> decltype(*(static_cast<typename T::Context*>(nullptr)));

template<typename T>
std::monostate context_type(...);

template<typename T>
using context_type_t = std::remove_reference_t<decltype(context_type<T>(nullptr))>;

} // namespace detail

struct Server : cg::Component
{
    template<typename Resolver>
    struct Types
    {
        template<typename Callback>
        class Reader
        {
        public:
            static constexpr bool sync = std::is_invocable_v<Callback, net::beast::error_code, std::size_t>;
            Reader(Callback &&callback) : callback{callback} {}

            template<typename B>
            inline auto operator () (net::beast::error_code ec, B &&b)
            {
                return callback(ec, std::forward<B>(b));
            }

            template<typename F>
            inline auto operator () (net::beast::error_code ec, std::size_t cn, F &&f)
            {
                return callback(ec, cn, std::forward<F>(f));
            }

            std::size_t data{0};
        private:
            Callback callback;
        };

        using Buffer = std::function<net::asio::mutable_buffer()>;

        template<typename Handler>
        class Context
        : public detail::context_type_t<typename Resolver::template Types<tag::async::Handler>>
        , public std::enable_shared_from_this<Context<Handler>>
        {
            Context(Context &&) = delete;
            Context(Context const &) = delete;
        public:
            Context(Handler handler, net::tcp::socket socket, std::uint64_t connectionid)
            : handler{handler}, socket{std::move(socket)}, connectionid(connectionid) {}

            ~Context()
            {
                if(auto it = response.find(net::http::field::connection); it != response.end() && !std::strncmp(it->value().data(), "keep-alive", 10) && !errorc)
                {
                    WEBLIB_LOG_INFO("Keep alive", (connectionid));
                    std::make_shared<Context<Handler>>(std::move(handler), std::move(socket), connectionid)->start();
                }
                else if(errorc)
                {
                    WEBLIB_LOG_INFO("Disconnected", (connectionid)(error, errorc.message()));
                }
                else
                {
                    WEBLIB_LOG_INFO("Disconnected", (connectionid));
                }
            }

            void start()
            {
                using namespace std::literals;
                net::http::async_read_header(socket, buffer_, request, [self=Context::shared_from_this()](net::beast::error_code ec, std::size_t){
                    if(ec && ec != net::http::error::end_of_stream && ec != net::http::error::body_limit)
                    {
                        self->error(ec);
                    }
                    else if(auto const &request = self->request.get(); ec == net::http::error::end_of_stream && !self->request.is_header_done())
                    {
                        self->errorc = ec;
                        self->header(net::http::field::connection, "close");
                        WEBLIB_LOG_INFO("Connection closed", (connectionid, self->connectionid));
                    }
                    else
                    {
                        self->header(net::http::field::server, "boost.beast");
                        self->response.version(self->request.get().version());
                        WEBLIB_LOG_INFO("Request", (connectionid, self->connectionid)(request));
                        switch(request.method())
                        {
#define WEBLIB_ASYNC_SERVER_TRY_METHOD(name) \
                        case net::http::verb::name: \
                            if constexpr (detail::handler_supports_method_v<Handler, std::shared_ptr<Context<Handler>>, method::name>) \
                            {\
                                if constexpr (detail::handler_supports_authentication_v<Handler, std::shared_ptr<Context<Handler>>>) \
                                {\
                                    if(auto a = self->handler.authenticate(self); true == a) self->handler.handle(method::name{}, self);\
                                    else if(false == a) self->status(net::http::status::unauthorized); \
                                    else self->body([self](auto, auto &&){ self->finish(); }); \
                                }\
                                else\
                                {\
                                    self->handler.handle(method::name{}, self); \
                                }\
                                if(1 == self.use_count()) \
                                {\
                                    WEBLIB_LOG_INFO("Finishing request", (connectionid, self->connectionid)(use_count, self.use_count())); \
                                    self->finish(); \
                                }\
                            }\
                            else\
                            {\
                                self->unsupported(); \
                            }\
                            break;

                        WEBLIB_ASYNC_SERVER_TRY_METHOD(acl)
                        WEBLIB_ASYNC_SERVER_TRY_METHOD(bind)
                        WEBLIB_ASYNC_SERVER_TRY_METHOD(checkout)
                        WEBLIB_ASYNC_SERVER_TRY_METHOD(connect)
                        WEBLIB_ASYNC_SERVER_TRY_METHOD(copy)
                        WEBLIB_ASYNC_SERVER_TRY_METHOD(delete_)
                        WEBLIB_ASYNC_SERVER_TRY_METHOD(get)
                        WEBLIB_ASYNC_SERVER_TRY_METHOD(head)
                        WEBLIB_ASYNC_SERVER_TRY_METHOD(link)
                        WEBLIB_ASYNC_SERVER_TRY_METHOD(lock)
                        WEBLIB_ASYNC_SERVER_TRY_METHOD(merge)
                        WEBLIB_ASYNC_SERVER_TRY_METHOD(mkactivity)
                        WEBLIB_ASYNC_SERVER_TRY_METHOD(mkcalendar)
                        WEBLIB_ASYNC_SERVER_TRY_METHOD(mkcol)
                        WEBLIB_ASYNC_SERVER_TRY_METHOD(move)
                        WEBLIB_ASYNC_SERVER_TRY_METHOD(msearch)
                        WEBLIB_ASYNC_SERVER_TRY_METHOD(notify)
                        WEBLIB_ASYNC_SERVER_TRY_METHOD(options)
                        WEBLIB_ASYNC_SERVER_TRY_METHOD(patch)
                        WEBLIB_ASYNC_SERVER_TRY_METHOD(post)
                        WEBLIB_ASYNC_SERVER_TRY_METHOD(propfind)
                        WEBLIB_ASYNC_SERVER_TRY_METHOD(proppatch)
                        WEBLIB_ASYNC_SERVER_TRY_METHOD(purge)
                        WEBLIB_ASYNC_SERVER_TRY_METHOD(put)
                        WEBLIB_ASYNC_SERVER_TRY_METHOD(rebind)
                        WEBLIB_ASYNC_SERVER_TRY_METHOD(report)
                        WEBLIB_ASYNC_SERVER_TRY_METHOD(search)
                        WEBLIB_ASYNC_SERVER_TRY_METHOD(subscribe)
                        WEBLIB_ASYNC_SERVER_TRY_METHOD(trace)
                        WEBLIB_ASYNC_SERVER_TRY_METHOD(unbind)
                        WEBLIB_ASYNC_SERVER_TRY_METHOD(unlink)
                        WEBLIB_ASYNC_SERVER_TRY_METHOD(unlock)
#undef WEBLIB_ASYNC_SERVER_TRY_METHOD
                        default:
                            self->unsupported();
                        }
                    }
                });
            }

            auto stream()
            {
                return net::beast::ostream(response.body());
            }

            auto method() const
            {
                return request.get().method();
            }

            void status(net::http::status status)
            {
                response.result(status);
            }

            auto const &headers() const
            {
                return request;
            }

            const char *header(net::http::field field) const
            {
                if(auto const it = request.get().find(field); it != request.get().end())
                {
                    return it->value().data();
                }
                return nullptr;
            }

            void header(net::beast::http::field field, char const *value)
            {
                response.set(field, value);
                if(field == net::http::field::transfer_encoding && !std::strcmp(value, "Chunked")) header_.emplace(response);
            }

            void header(net::beast::http::field field, std::string const &value)
            {
                response.set(field, value);
                if(field == net::http::field::transfer_encoding && value == "Chunked") header_.emplace(response);
            }

            void header(net::beast::http::field field, boost::string_view const &value)
            {
                response.set(field, value);
                if(field == net::http::field::transfer_encoding && value == "Chunked") header_.emplace(response);
            }

            auto target() const
            {
                if(target_)
                {
                    return net::beast::string_view(target_->c_str(), target_->size());
                }
                return request.get().target();
            }

            void target(std::string &&url)
            {
                target_.emplace(url);
            }

            void length(std::size_t l)
            {
                response.content_length(l);
            }

            std::optional<std::uint64_t> length() const
            {
                if(auto const it = request.get().find(net::beast::http::field::content_length); it != request.get().end())
                {
                    return std::atoll(it->value().data());
                }
                return std::nullopt;
            }

            std::optional<std::uint64_t> expected_length() const
            {
                if(auto const it = request.get().find("X-Expected-Entity-Length"); it != request.get().end())
                {
                    return std::atoll(it->value().data());
                }
                return std::nullopt;
            }

            template<typename Callback>
            void body(Callback &&callback, boost::logic::tribool cont = boost::logic::indeterminate)
            {
                if(auto e = header(net::http::field::expect); (e && 100 == std::atoi(e) && boost::logic::indeterminate(cont)) || true == cont)
                {
                    status(net::http::status::continue_);
                    header(net::http::field::connection, "keep-alive");
                    send([
                        self=Context::shared_from_this(),
                        reader=std::make_shared<Reader<Callback>>(std::forward<Callback>(callback))
                    ](net::beast::error_code ec, std::size_t cn) mutable {
                        if(ec)
                        {
                            (*reader)(ec, self->buffer_);
                        }
                        else
                        {
                            self->body_(reader);
                        }
                    });
                }
                else
                {
                    body_(std::make_shared<Callback>(std::forward<Callback>(callback)));
                }
            }

            template<typename Callback>
            void body(net::asio::mutable_buffer buf, Callback &&callback, boost::logic::tribool cont = boost::logic::indeterminate)
            {
                if(auto e = header(net::http::field::expect); (e && 100 == std::atoi(e) && boost::logic::indeterminate(cont)) || true == cont)
                {
                    status(net::http::status::continue_);
                    header(net::http::field::connection, "keep-alive");
                    send([
                        buf,
                        self=Context::shared_from_this(),
                        reader=std::make_shared<Reader<Callback>>(std::forward<Callback>(callback))
                    ](net::beast::error_code ec, std::size_t sz) mutable {
                        if(ec)
                        {
                            if constexpr (Reader<Callback>::sync)
                            {
                                (*reader)(ec, sz);
                            }
                            else
                            {
                                (*reader)(ec, sz, [](bool) -> void {});
                            }
                        }
                        else
                        {
                            self->body_(buf, reader);
                        }
                    });
                }
                else
                {
                    body_(buf, std::make_shared<Reader<Callback>>(std::forward<Callback>(callback)));
                }
            }

            template<typename Callback>
            void body(Buffer &&buf, Callback &&callback, boost::logic::tribool cont = boost::logic::indeterminate)
            {
                if(auto e = header(net::http::field::expect); (e && 100 == std::atoi(e) && boost::logic::indeterminate(cont)) || true == cont)
                {
                    status(net::http::status::continue_);
                    header(net::http::field::connection, "keep-alive");
                    send([
                        buf=std::make_shared<Buffer>(std::forward<Buffer>(buf)),
                        self=Context::shared_from_this(),
                        reader=std::make_shared<Reader<Callback>>(std::forward<Callback>(callback))
                    ](net::beast::error_code ec, std::size_t sz) mutable {
                        if(ec)
                        {
                            if constexpr (Reader<Callback>::sync)
                            {
                                (*reader)(ec, sz);
                            }
                            else
                            {
                                (*reader)(ec, sz, [](bool) -> void {});
                            }
                        }
                        else
                        {
                            self->body_(buf, reader);
                        }
                    });
                }
                else
                {
                    body_(std::make_shared<Buffer>(std::forward<Buffer>(buf)), std::make_shared<Reader<Callback>>(std::forward<Callback>(callback)));
                }
            }

            template<typename Callback>
            void body(net::asio::mutable_buffer buf, std::size_t const * const length, Callback &&callback, boost::logic::tribool cont = boost::logic::indeterminate)
            {
                if(auto e = header(net::http::field::expect); (e && 100 == std::atoi(e) && boost::logic::indeterminate(cont)) || true == cont)
                {
                    status(net::http::status::continue_);
                    header(net::http::field::connection, "keep-alive");
                    send([
                        buf,
                        length,
                        self=Context::shared_from_this(),
                        reader=std::make_shared<Reader<Callback>>(std::forward<Callback>(callback))
                    ](net::beast::error_code ec, std::size_t sz) mutable {
                        if(ec)
                        {
                            if constexpr (Reader<Callback>::sync)
                            {
                                (*reader)(ec, sz);
                            }
                            else
                            {
                                (*reader)(ec, sz, [](bool) -> void {});
                            }
                        }
                        else
                        {
                            self->body_(buf, length, reader);
                        }
                    });
                }
                else
                {
                    body_(buf, length, std::make_shared<Reader<Callback>>(std::forward<Callback>(callback)));
                }
            }

            template<typename Callback>
            void send(Callback &&callback)
            {
                WEBLIB_LOG_INFO("Response", (connectionid)(response));
                if(header_)
                {
                    net::http::async_write_header(socket, *header_, std::forward<Callback>(callback));
                }
                else
                {
                    net::http::async_write(socket, response, std::forward<Callback>(callback));
                }
            }

            template<typename Callback>
            void send(std::size_t cn, Callback &&callback)
            {
                net::asio::async_write(socket, buffer_, net::asio::transfer_exactly(cn), std::forward<Callback>(callback));
            }

            template<typename Callback>
            void send(net::asio::mutable_buffer buf, std::size_t cn, Callback &&callback)
            {
                net::asio::async_write(socket, buf, net::asio::transfer_exactly(cn), std::forward<Callback>(callback));
            }

            template<typename Callback>
            void send(net::asio::const_buffer buf, std::size_t cn, Callback &&callback)
            {
                net::asio::async_write(socket, buf, net::asio::transfer_exactly(cn), std::forward<Callback>(callback));
            }

            void error(net::beast::error_code ec)
            {
                LOG_ERROR("Error", (connectionid)(error, ec.message())(code, ec.value()));
                errorc = ec;
            }

            bool keep_alive() const
            {
                if(auto ka = header(boost::beast::http::field::connection); ka)
                {
                    return !std::strncmp(ka, "keep-alive", 10) || !std::strncmp(ka, "Keep-Alive", 10);
                }
                return false;
            }

            void finish()
            {
                auto self = Context::shared_from_this();
                response.content_length(response.body().size());
                if(keep_alive())
                {
                    header(net::http::field::connection, "keep-alive");
                }
                else
                {
                    header(net::http::field::connection, "close");
                }
                WEBLIB_LOG_INFO("Response", (connectionid)(response));
                net::http::async_write(socket, response, [self](net::beast::error_code ec, std::size_t) {
                    self->errorc = ec;
                });
            }

            inline std::uint64_t id() const { return connectionid; }

        private:
            template<typename Reader>
            void body_(std::shared_ptr<Reader> reader)
            {
                if(auto len = length(); len)
                {
                    if(*len > buffer_.data().size())
                    {
                        auto const expect = *len - buffer_.data().size();
                        net::asio::async_read(socket, buffer_, net::asio::transfer_exactly(expect),
                            [self=Context::shared_from_this(), reader](net::beast::error_code ec, std::size_t c) mutable {
                                (*reader)(ec, self->buffer_);
                            });
                    }
                    else
                    {
                        (*reader)(net::beast::error_code{}, buffer_);
                    }
                }
                else
                {
                    net::asio::async_read(socket, buffer_, net::asio::transfer_all(),
                        [self=Context::shared_from_this(), reader](net::beast::error_code ec, std::size_t c) mutable {
                            (*reader)(ec, self->buffer_);
                        });
                }
            }

            template<typename Reader>
            void body_(net::asio::mutable_buffer buf, std::shared_ptr<Reader> reader, std::enable_if_t<Reader::sync, void*> = nullptr)
            {
                std::istream stream(&buffer_);
                for(std::size_t s = 0; s < buffer_.size();)
                {
                    auto const sz = std::min(buffer_.size() - s, buf.size());
                    stream.read(reinterpret_cast<char*>(buf.data()), sz);
                    if(!(*reader)(net::beast::error_code{}, sz))
                    {
                        return;
                    }
                    s += sz;
                }

                auto const len = length();
                body_(buf, reader, len ? *len - buffer_.size() : std::numeric_limits<std::size_t>::max());
            }

            template<typename Reader>
            void body_(net::asio::mutable_buffer buf, std::shared_ptr<Reader> reader, std::enable_if_t<!Reader::sync, void*> = nullptr)
            {
                auto read = boost::hana::fix([context=Context::shared_from_this(), buf, reader](auto &&self, bool result) mutable -> void {
                    if(result)
                    {
                        if(reader->data < context->buffer_.size())
                        {
                            auto stream = std::istream(&context->buffer_);
                            auto const sz = std::min(context->buffer_.size() - reader->data, buf.size());
                            stream.read(reinterpret_cast<char*>(buf.data()), sz);
                            reader->data += sz;
                            (*reader)(net::beast::error_code{}, sz, std::forward<decltype(self)>(self));
                        }
                        else
                        {
                            auto const len = context->length();
                            context->body_(buf, reader, len ? *len - reader->data : std::numeric_limits<std::size_t>::max());
                        }
                    }
                    else
                    {
                        LOG_WARN("Body callback returned an error", (connectionid, context->connectionid)(request, context->request.get()));
                    }
                });
                read(true);
            }

            template<typename Reader>
            void body_(std::shared_ptr<Buffer> buf, std::shared_ptr<Reader> reader, std::enable_if_t<Reader::sync, void*> = nullptr)
            {
                std::istream stream(&buffer_);
                for(std::size_t s = 0; s < buffer_.size();)
                {
                    auto const sz = std::min(buffer_.size() - s, (*buf)().size());
                    stream.read(reinterpret_cast<char*>((*buf)().data()), sz);
                    if(!(*reader)(net::beast::error_code{}, sz))
                    {
                        return;
                    }
                    s += sz;
                }

                auto const len = length();
                body_(buf, reader, len ? *len - buffer_.size() : std::numeric_limits<std::size_t>::max());
            }

            template<typename Reader>
            void body_(net::asio::mutable_buffer buf, std::size_t const * const length, std::shared_ptr<Reader> reader, std::enable_if_t<!Reader::sync, void*> = nullptr)
            {
                auto read = boost::hana::fix([context=Context::shared_from_this(), length, buf, reader](auto &&self, bool result) mutable -> void {
                    if(result)
                    {
                        if(reader->data < context->buffer_.size())
                        {
                            auto stream = std::istream(&context->buffer_);
                            auto const sz = std::min(context->buffer_.size() - reader->data, buf.size());
                            stream.read(reinterpret_cast<char*>(buf.data()), sz);
                            reader->data += sz;
                            (*reader)(net::beast::error_code{}, sz, std::forward<decltype(self)>(self));
                        }
                        else
                        {
                            context->body_(buf, reader, length);
                        }
                    }
                    else
                    {
                        LOG_WARN("Body callback returned an error", (connectionid, context->connectionid)(request, context->request.get()));
                    }
                });
                read(true);
            }

            template<typename Reader>
            void body_(net::asio::mutable_buffer buf, std::shared_ptr<Reader> reader, std::size_t expect, std::enable_if_t<Reader::sync, void*> = nullptr)
            {
                auto const transfer = std::min(buf.size(), expect);
                net::asio::async_read(socket, buf, net::asio::transfer_exactly(transfer),
                    [context=Context::shared_from_this(), buf, reader, expect](net::beast::error_code ec, std::size_t sz) mutable {
                        auto const left = (expect - sz);
                        if(auto r = (*reader)(left ? ec : net::http::error::end_of_stream, sz); r && left)
                        {
                            context->body_(buf, reader, left);
                        }
                        else if(!r)
                        {
                            LOG_WARN("Body callback returned an error", (connectionid, context->connectionid)(request, context->request.get()));
                        }
                    });
            }

            template<typename Reader>
            void body_(std::shared_ptr<Buffer> buf, std::shared_ptr<Reader> reader, std::size_t expect, std::enable_if_t<Reader::sync, void*> = nullptr)
            {
                auto const transfer = std::min((*buf)().size(), expect);
                net::asio::async_read(socket, (*buf)(), net::asio::transfer_exactly(transfer),
                    [context=Context::shared_from_this(), buf, reader, expect](net::beast::error_code ec, std::size_t sz) mutable {
                        auto const left = (expect - sz);
                        if(auto r = (*reader)(left ? ec : net::http::error::end_of_stream, sz); r && left)
                        {
                            context->body_(buf, reader, left);
                        }
                        else if(!r)
                        {
                            LOG_WARN("Body callback returned an error", (connectionid, context->connectionid)(request, context->request.get()));
                        }
                    });
            }

            template<typename Reader> 
            void body_(net::asio::mutable_buffer buf, std::shared_ptr<Reader> reader, std::size_t expect, std::enable_if_t<!Reader::sync, void*> = nullptr)
            {
                reader->data = expect;
                auto read = boost::hana::fix([context=Context::shared_from_this(), buf, reader](auto &&self, bool result) mutable -> void {
                    using Self = std::decay_t<decltype(self)>;
                    if(result && reader->data)
                    {
                        auto const transfer = std::min(buf.size(), reader->data);
                        net::asio::async_read(context->socket, buf, net::asio::transfer_exactly(transfer),
                            [context, buf, reader, self=std::forward<Self>(self)](net::beast::error_code ec, std::size_t sz) mutable -> void {
                                reader->data -= sz;
                                (*reader)(reader->data ? ec : net::http::error::end_of_stream, sz, [self=std::forward<Self>(self)](bool result) mutable {
                                    self(result);
                                });
                            });
                    }
                    else if(!result)
                    {
                        LOG_WARN("Body callback returned an error", (connectionid, context->connectionid)(request, context->request.get()));
                    }
                    else
                    {
                        (*reader)(net::http::error::end_of_stream, 0, [](bool){});
                    }
                });
                read(true);
            }

            template<typename Reader>
            void body_(net::asio::mutable_buffer buf, std::shared_ptr<Reader> reader, std::size_t const * const expect, std::enable_if_t<!Reader::sync, void*> = nullptr)
            {
                auto read = boost::hana::fix([context=Context::shared_from_this(), buf, reader, expect](auto &&self, bool result) mutable -> void {
                    using Self = std::decay_t<decltype(self)>;
                    if(result && *expect)
                    {
                        auto const transfer = std::min(buf.size(), *expect);
                        net::asio::async_read(context->socket, buf, net::asio::transfer_exactly(transfer),
                            [context, buf, reader, expect, self=std::forward<Self>(self)](net::beast::error_code ec, std::size_t sz) mutable -> void {
                                (*reader)(*expect ? ec : net::http::error::end_of_stream, sz, [self=std::forward<Self>(self)](bool result) mutable {
                                    self(result);
                                });
                            });
                    }
                    else if(!result)
                    {
                        LOG_WARN("Body callback returned an error", (connectionid, context->connectionid)(request, context->request.get()));
                    }
                    else
                    {
                        (*reader)(net::http::error::end_of_stream, 0, [](bool){});
                    }
                });
                read(true);
            }

            void unsupported()
            {
                if(request.get().method() == net::beast::http::verb::unknown)
                {
                    response.result(net::beast::http::status::bad_request);
                    net::beast::ostream(response.body()) << "Invalid method " << request.get().method_string() << '\n';
                }
                else
                {
                    response.result(net::beast::http::status::method_not_allowed);
                    net::beast::ostream(response.body()) << "Method " << request.get().method_string() << " not supported";
                }
                finish();
            }

        private:
            Handler handler;
            net::tcp::socket socket;
            std::uint64_t connectionid;
            net::asio::streambuf buffer_;
            std::optional<std::string> target_;
            mutable net::beast::error_code errorc{};
            net::beast::http::response<net::beast::http::dynamic_body> response;
            net::beast::http::parser<true, net::beast::http::string_body> request;
            std::optional<net::http::response_serializer<net::beast::http::dynamic_body>> header_;
        };
    };

    struct Impl
    {
        template<typename Base>
        struct Interface : Base
        {
            int run()
            {
                net::asio::io_context ioc{1};
                if constexpr (Interface<Base>::template has_remote<msgbus::tag::Msgbus>) {
                    this->remote(msgbus::tag::Msgbus{}).stopped([&ioc](){
                        ioc.stop();
                    });
                }

                this->apply(tag::async::Context{}, [&ioc](auto remote) {
                    remote.context(ioc);
                });

                auto const addr = net::asio::ip::make_address(this->state.addr);
                unsigned short const port = static_cast<unsigned short>(this->state.port);

                net::tcp::acceptor acceptor {ioc, {addr, port}};
                net::tcp::socket   socket   {ioc};

                server(acceptor, socket);
                WEBLIB_LOG_INFO("Server started");
                return 0 < ioc.run();
            }
        private:
            void server(net::tcp::acceptor &acceptor, net::tcp::socket &socket)
            {
                using Handler = decltype(this->remote(weblib::tag::async::Handler{}));
                acceptor.async_accept(socket, [&](net::beast::error_code ec){
                    if(!ec)
                    {
                        std::make_shared<typename Base::template Context<Handler>>(
                            this->remote(weblib::tag::async::Handler{}), std::move(socket), ++this->state.connectionid
                        )->start();
                    }
                    this->server(acceptor, socket);
                });
            }
        };
    };

    template<typename>
    struct State
    {
        int const port;
        std::string const addr;
        std::uint64_t connectionid{0};
        CONFIG_DECLARE_STATE_CONSTRUCTORS(addr, port) {}
    };

    using Services = ct::tuple<Impl>;
};

} // namespace weblib::async
