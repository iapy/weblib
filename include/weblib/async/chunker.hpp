#pragma once
#include <weblib/tags.hpp>
#include <cg/component.hpp>
#include <memory>

#if 0
#define IF_CHUNKER_DEBUG(...) std::cout << __LINE__ << ": " << __VA_ARGS__ << '\n'
#else
#define IF_CHUNKER_DEBUG(...)
#endif

namespace weblib::async {

struct Chunker : cg::Component
{
    template<typename>
    struct Types
    {
        struct Parser
        {
            enum class State : uint16_t
            {
                SIZE   = 0,
                BODY   = 1,
                ERROR  = 2,
                FINISH = 3,
                CR     = 4, // unused
                CRE    = 5,
                CRF    = 6,
                LF     = 7,
                LFE    = 8,
                LFF    = 9
            };

            State state{State::SIZE};
            std::size_t size_read{0};
            std::size_t chunk_size{0};
            std::size_t remained_size{0};
        };

        // The tail of the chunk encoded message
        static constexpr std::string_view TERMINATE{"\r\n0\r\n\r\n"};
    };

    struct Impl
    {
        template<typename Base>
        struct Interface : Base
        {
            template<typename Callback, typename Context>
            void body(net::asio::mutable_buffer buf, char const *data, typename Base::Parser &parser, std::shared_ptr<Context> context, Callback &&callback, boost::logic::tribool cont = boost::logic::indeterminate)
            {
                if(auto es = context->expected_length(); es)
                {
                    parser.remained_size = *es + Base::TERMINATE.size();
                    context->body(buf, &parser.remained_size, [self=*this, buf, data, &parser, context, callback=std::make_shared<Callback>(std::forward<Callback>(callback))](net::beast::error_code ec, std::size_t cn, auto &&next) mutable {
                        self.body_(buf, data, 0, parser, context, callback, ec, cn, std::forward<decltype(next)>(next));
                    }, cont);
                }
                else
                {
                    parser.remained_size = std::numeric_limits<std::size_t>::max();
                    context->body(buf, [self=*this, buf, data, &parser, context, callback=std::make_shared<Callback>(std::forward<Callback>(callback))](net::beast::error_code ec, std::size_t cn, auto &&next) mutable {
                        self.body_(buf, data, 0, parser, context, callback, ec, cn, std::forward<decltype(next)>(next));
                    }, cont);
                }
            }
        private:
            template<typename Callback, typename Context, typename Next>
            void body_(net::asio::mutable_buffer buf, char const *data, std::size_t off, typename Base::Parser &parser, std::shared_ptr<Context> context, std::shared_ptr<Callback> callback, net::beast::error_code ec, std::size_t cn, Next &&next)
            {
                IF_CHUNKER_DEBUG("off=" << off << " cn=" << cn << " st=" << static_cast<uint16_t>(parser.state) << " ch=" << parser.chunk_size);
                for(std::size_t i = off; i < cn && parser.state != Base::Parser::State::ERROR; ++i)
                {
                    switch(parser.state)
                    {
                    case Base::Parser::State::SIZE:
                        ++parser.size_read;
                        switch(data[i])
                        {
                        case '0': case '1': case '2': case '3': case '4':
                        case '5': case '6': case '7': case '8': case '9':
                            parser.chunk_size <<= 4;
                            parser.chunk_size += (data[i] - '0');
                            break;
                        case 'a': case 'b': case 'c':
                        case 'd': case 'e': case 'f':
                            parser.chunk_size <<= 4;
                            parser.chunk_size += (data[i] - 'a') + 10;
                            break;
                        case 'A': case 'B': case 'C':
                        case 'D': case 'E': case 'F':
                            parser.chunk_size <<= 4;
                            parser.chunk_size += (data[i] - 'A') + 10;
                            break;
                        case '\r':
                            parser.state = Base::Parser::State::LF;
                            break;
                        default:
                            IF_CHUNKER_DEBUG("Unexpected character = " << int(data[i]) << " pos=" << (i - off));
                            parser.state = Base::Parser::State::ERROR;
                            break;
                        }
                        break;
                    case Base::Parser::State::LF:
                        switch(data[i])
                        {
                        case '\n':
                            IF_CHUNKER_DEBUG("Chunk size = " << parser.chunk_size << " i=" << i << " cn=" << cn);
                            if(parser.chunk_size == 0)
                            {
                                parser.state = Base::Parser::State::CRF;
                            }
                            else
                            {
                                parser.state = Base::Parser::State::BODY;
                            }
                            break;
                        default:
                            IF_CHUNKER_DEBUG("Expected \\n got " << int(data[i]) << " pos=" << (i - off));
                            parser.state = Base::Parser::State::ERROR;
                            break;
                        }
                        break;
                    case Base::Parser::State::BODY:
                        if(auto const left = (cn - i); left > parser.chunk_size)
                        {
                            // The chunk fits in the buffer
                            parser.state = Base::Parser::State::CRE;
                            parser.remained_size -= parser.chunk_size;
                            std::size_t const cs = std::exchange(parser.chunk_size, 0);
                            IF_CHUNKER_DEBUG("Feed chunk=" << cs << " cn=" << cn << " cs=" << cs << " off=" << i << " left=" << parser.remained_size);
                            (*callback)(net::beast::error_code{}, i, cs, [self=*this, data, context, callback, buf, off=(i + cs), cn, &parser, next=std::forward<Next>(next)](bool result) mutable {
                                self.body_(buf, data, off, parser, context, callback, net::beast::error_code{}, cn, std::forward<Next>(next));
                            });
                            return;
                        }
                        else
                        {
                            // The chunk doesn't fit in the buffer
                            parser.remained_size -= left;
                            IF_CHUNKER_DEBUG("Feed chunk=" << left << " size=" << parser.chunk_size << " left=" << parser.remained_size);
                            if(!(parser.chunk_size -= left))
                            {
                                parser.state = Base::Parser::State::CRE;
                            }
                            (*callback)(net::beast::error_code{}, i, left, std::forward<Next>(next));
                            return;
                        }
                        break;
                    case Base::Parser::State::CRE:
                        switch(data[i])
                        {
                        case '\r':
                            parser.state = Base::Parser::State::LFE;
                            break;
                        default:
                            IF_CHUNKER_DEBUG("Expected \\r got " << int(data[i]) << " pos=" << (i - off));
                            parser.state = Base::Parser::State::ERROR;
                            break;
                        }
                        break;
                    case Base::Parser::State::CRF:
                        switch(data[i])
                        {
                        case '\r':
                            parser.state = Base::Parser::State::LFF;
                            break;
                        default:
                            IF_CHUNKER_DEBUG("Expected \\r got " << int(data[i]) << " pos=" << (i - off));
                            parser.state = Base::Parser::State::ERROR;
                            break;
                        }
                        break;
                    case Base::Parser::State::LFE:
                        switch(data[i])
                        {
                        case '\n':
                            parser.state = Base::Parser::State::SIZE;
                            parser.size_read = 0;
                            break;
                        default:
                            IF_CHUNKER_DEBUG("Expected \\n got " << int(data[i]) << " pos=" << (i - off));
                            parser.state = Base::Parser::State::ERROR;
                            break;
                        }
                        break;
                    case Base::Parser::State::LFF:
                        switch(data[i])
                        {
                        case '\n':
                            parser.state = Base::Parser::State::FINISH;
                            break;
                        default:
                            IF_CHUNKER_DEBUG("Expected \\n got " << int(data[i]) << " pos=" << (i - off));
                            parser.state = Base::Parser::State::ERROR;
                            break;
                        }
                        break;
                    default:
                        parser.state = Base::Parser::State::ERROR;
                        break;
                    }
                }

                if(parser.state == Base::Parser::State::ERROR)
                {
                    IF_CHUNKER_DEBUG("Parser error"); 
                    (*callback)(net::http::error::bad_chunk, off, cn - off, std::forward<Next>(next));
                }
                else if(ec && ec != net::http::error::end_of_stream)
                {
                    IF_CHUNKER_DEBUG("Error " << ec.message()); 
                    (*callback)(ec, off, cn - off, std::forward<Next>(next));
                }
                else if(parser.state == Base::Parser::State::FINISH)
                {
                    IF_CHUNKER_DEBUG("Finish"); 
                    (*callback)(net::http::error::end_of_stream, off, 0, std::forward<Next>(next));
                }
                else if(parser.chunk_size && parser.chunk_size >= (cn - off) && (cn - off) && parser.state == Base::Parser::State::BODY)
                {
                    if(!(parser.chunk_size -= (cn - off)))
                    {
                        parser.state = Base::Parser::State::CRE;
                    }
                    parser.remained_size -= (cn - off);
                    IF_CHUNKER_DEBUG("All chunk " << (cn - off) << " left=" << parser.remained_size);
                    (*callback)(net::beast::error_code{}, off, (cn - off), std::forward<Next>(next));
                }
                else
                {
                    IF_CHUNKER_DEBUG("Next state=" << static_cast<uint16_t>(parser.state));
                    if(parser.remained_size <= Base::TERMINATE.size())
                    {
                        switch(parser.state)
                        {
                            case Base::Parser::State::LFE:
                                parser.remained_size = 6; break;
                            case Base::Parser::State::SIZE:
                                parser.remained_size = 5 - parser.size_read; break;
                            case Base::Parser::State::CR:
                                parser.remained_size = 3; break;
                            case Base::Parser::State::LF:
                                parser.remained_size = 2; break;
                            case Base::Parser::State::CRF:
                                parser.remained_size = 1; break;
                            default:
                                break;
                        }
                    }
                    next(true);
                }
            }
        };
    };

    using Ports = ct::map<
        ct::pair<tag::async::Chunker, Impl>
    >;
};

} // namespace weblib::async

#undef IF_CHUNKER_DEBUG
