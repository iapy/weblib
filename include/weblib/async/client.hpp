#pragma once
#include <weblib/net.hpp>
#include <optional>
#include <iostream>

#include <boost/asio/read.hpp>
#include <boost/asio/read_until.hpp>

namespace weblib::async {

class Client
{
public:
    using Request = net::http::request<net::http::string_body>;
    using Response = net::http::parser<false, net::http::string_body>;

#ifdef BOOST_ASIO_ANY_IO_EXECUTOR_HPP
    using Executor = net::asio::any_io_executor;
#else
    using Executor = net::asio::executor;
#endif

public:
    Client(net::beast::tcp_stream &&);
    Client(Executor const &ex, net::tcp::resolver::results_type const &);

    inline Request &request(net::http::verb method, std::string &&path)
    {
        return request_.emplace(method, path, 11);
    }

    inline net::http::response<net::http::string_body> &response()
    {
        return response_.emplace().get();
    }

    inline bool connected() const
    {
        return connected_;
    }

    inline net::asio::streambuf::const_buffers_type buffer(std::size_t off = 0)
    {
        buffer_->pubseekpos(off);
        return buffer_->data();
    }

    template<typename Callback>
    void send(Callback &&callback)
    {
        net::http::async_write(stream_, *request_, std::forward<Callback>(callback));
    }

    template<typename Callback>
    void recv(Callback &&callback)
    {
        net::http::async_read(stream_, buffer_.emplace(), *response_, std::forward<Callback>(callback));
    }

    template<typename Buffer, typename Callback>
    void recv(Buffer &buffer, std::size_t cn, Callback &&callback)
    {
        net::asio::async_read(stream_, buffer, net::asio::transfer_exactly(cn), std::forward<Callback>(callback));
    }

    template<typename Buffer, typename Callback>
    void send(Buffer &buffer, std::size_t cn, Callback &&callback)
    {
        net::asio::async_write(stream_, buffer, net::asio::transfer_exactly(cn), std::forward<Callback>(callback));
    }

    template<typename Callback>
    void head(Callback &&callback)
    {
        net::http::async_read_header(stream_, buffer_.emplace(), *response_, std::forward<Callback>(callback)); 
    }

    template<typename Callback>
    void done(Callback &&callback)
    {
        stream_.socket().shutdown(net::tcp::socket::shutdown_send);
        net::http::async_read(stream_, buffer_.emplace(), *response_, std::forward<Callback>(callback));
    }

    const char *header(net::http::field field) const
    {
        if(auto const it = response_->get().find(field); it != response_->get().end())
        {
            return it->value().data();
        }
        return nullptr;
    }

private:
    bool connected_;
    net::beast::tcp_stream stream_;
    std::optional<Request> request_;
    std::optional<Response> response_;
    std::optional<net::asio::streambuf> buffer_;
};

} // namespace weblib::async

