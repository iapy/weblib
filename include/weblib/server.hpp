#pragma once

#include <weblib/tags.hpp>
#include <ct/transform.hpp>
#include <cg/component.hpp>
#include <cg/connect.hpp>
#include <cg/config.hpp>
#include <logger/logger.hpp>

#define CROW_MAIN
#include <crow.h>

namespace weblib {
namespace detail {

template<typename T>
auto declares_middleware(T*) -> decltype(
    static_cast<typename T::Middleware*>(nullptr), std::true_type{}
);

template<typename T>
std::false_type declares_middleware(...);

template<typename T>
constexpr bool declares_middleware_v = decltype(declares_middleware<T>(0))::value;

template<typename Types, bool = declares_middleware_v<Types>>
struct middlewares_impl
{
    using type = ct::tuple<typename Types::Middleware>;
};

template<typename Types>
struct middlewares_impl<Types, false>
{
    using type = ct::tuple<>;
};

template<typename Types>
struct middlewares : middlewares_impl<Types> {};

CT_TYPE(crow_app);

template<typename ...Middlewares>
struct crow_app<ct::tuple<Middlewares...>>
{
    using type = crow::App<Middlewares...>;
};

}

struct Server : cg::Component
{
    template<typename Resolver>
    struct Types
    {
        using Middlewares = ct::transform_t<detail::middlewares, typename Resolver::template TypesTuple<tag::Handler>>;

        struct LogHandlerBase : crow::ILogHandler
        {
            std::atomic<bool> enabled;

            LogHandlerBase(bool enabled): enabled{enabled} {}
            virtual ~LogHandlerBase() = default;
        };

        template<typename Interface>
        class LogHandler : public LogHandlerBase
        {
        public:
            LogHandler(Interface *interface, bool enable)
            : interface{interface}
            , LogHandlerBase{enable}
            {}

            void log(std::string message, crow::LogLevel level) override
            {
                if(LogHandlerBase::enabled)
                {
                    switch(level)
                    {
                        case crow::LogLevel::Debug:
                            interface->remote(logger::tag::Logger{}).message(logger::tag::Logger::DEBUG, __FILE__, __LINE__, message.c_str());
                            break;
                        case crow::LogLevel::Info:
                            interface->remote(logger::tag::Logger{}).message(logger::tag::Logger::INFO, __FILE__, __LINE__, message.c_str());
                            break;
                        case crow::LogLevel::Warning:
                            interface->remote(logger::tag::Logger{}).message(logger::tag::Logger::WARN, __FILE__, __LINE__, message.c_str());
                            break;
                        case crow::LogLevel::Error:
                            interface->remote(logger::tag::Logger{}).message(logger::tag::Logger::ERROR, __FILE__, __LINE__, message.c_str());
                            break;
                        case crow::LogLevel::Critical:
                            interface->remote(logger::tag::Logger{}).message(logger::tag::Logger::FATAL, __FILE__, __LINE__, message.c_str());
                            break;
                        default: break;
                    }
                }
            }

        private:
            Interface *interface;
        };

        struct LogMessage
        {
            BOOST_HANA_DEFINE_STRUCT(LogMessage,
                (int, level),
                (std::string, service),
                (int, line),
                (std::string, message),
                (nlohmann::json, json)
            );
        };
    };

    struct Impl
    {
        template<typename Base>
        struct Interface : Base
        {
            int run()
            {
                using Self = Interface<Base>;
                typename detail::crow_app<typename Base::Middlewares>::type app;

                this->apply(tag::Handler{}, [&app](auto handler){
                    handler.setup(app);
                });

                crow::Blueprint blueprint{"logger"};
                if constexpr (Interface<Base>::template has_remote<logger::tag::Logger>)
                {
                    using LogHandler = typename Base::template LogHandler<Self>;

                    this->state.logger = std::make_unique<LogHandler>(this, this->state.enable_logger);
                    crow::logger::setHandler(this->state.logger.get());

                    CROW_BP_ROUTE(blueprint, "/").methods("POST"_method)([this](crow::request const &request){
                        typename Base::LogMessage message;
                        jsonio::from(nlohmann::json::parse(request.body), message);

                        std::optional<nlohmann::json> data;
                        if(!message.json.is_null())
                        {
                            data = message.json;
                        }

                        this->remote(logger::tag::Logger{}).message(
                            static_cast<logger::tag::Logger>(message.level),
                            this->state.file(std::move(message.service)),
                            message.line, message.message.c_str(), std::move(data)
                        );

                        return crow::response{};
                    });

                    CROW_BP_ROUTE(blueprint, "/").methods("PUT"_method)([this](){
                        if(auto path = this->remote(logger::tag::Logger{}).rotate(); path)
                        {
                            return crow::response{crow::status::OK, *path};
                        }
                        else
                        {
                            return crow::response{crow::status::INTERNAL_SERVER_ERROR};
                        }
                    });

                    CROW_BP_ROUTE(blueprint, "/").methods("GET"_method)([this](){
                        return crow::response{crow::status::OK, this->state.logger->enabled ? "true" : "false"};
                    });

                    CROW_BP_ROUTE(blueprint, "/0").methods("POST"_method)([this](){
                        this->state.logger->enabled = false;
                        return crow::response{crow::status::OK};
                    });

                    CROW_BP_ROUTE(blueprint, "/1").methods("POST"_method)([this](){
                        this->state.logger->enabled = true;
                        return crow::response{crow::status::OK};
                    });

                    app.signal_clear();
                    this->remote(logger::tag::Logger{}).stopped([&app](){
                        app.stop();
                    });

                    app.register_blueprint(blueprint);
                    LOG_INFO("Starting webserver");
                }
                else if constexpr (Self::template has_remote<msgbus::tag::Msgbus>) {
                    this->remote(msgbus::tag::Msgbus{}).stopped([&app](){
                        app.stop();
                    });
                }

                app.bindaddr(this->state.addr).port(this->state.port).concurrency(this->state.threads).run();
                return 0;
            }

            friend typename Base::template LogHandler<Interface<Base>>;
        };
    };

    struct StateBase
    {
        char const *file(std::string &&service) const;
    };

    template<typename Types>
    struct State : StateBase
    {
        int const port;
        std::string const addr;
        std::uint16_t threads;
        bool const enable_logger;
        CONFIG_DECLARE_STATE_CONSTRUCTORS(addr, port, threads, enable_logger) {}

        std::unique_ptr<typename Types::LogHandlerBase> logger;
    };

    using Services = ct::tuple<Impl>;
};

template<typename ...Components>
using Handlers = cg::Connect<Server, cg::Group<Components...>, tag::Handler>;

} // namespace weblib

namespace cg
{
    template<>
    struct json_type<crow::Blueprint>
    {
        using type = std::string;
    };
}
