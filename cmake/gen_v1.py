#!/usr/bin/env python3
import os, sys
import subprocess


class Preprocessor(object):
    GEN = 0
    CPP = 1

    def __init__(self, inp, out):
        self.inp = inp
        self.out = out
        self.code = Preprocessor.CPP
        self.strp = False

    def __enter__(self):
        os.makedirs(os.path.dirname(self.out), exist_ok=True)

        self.inp = open(self.inp, 'r')
        self.out = open(self.out, 'w')

        return self

    def __exit__(self, *args):
        self.inp.close()
        self.out.close()

    def _preprocess(self, i, line):
        if not line.startswith('"'):
            if self.code == Preprocessor.GEN:
                self.code = Preprocessor.CPP
                self.out.write(')*" "\\n";\n')
            self.out.write(line)
        else:
            pp = line[1:].replace('"{', ')*" <<').replace('}"', '<< R"*(')
            if self.code == Preprocessor.CPP:
                self.code = Preprocessor.GEN
                self.out.write(f'stream << R"*({pp.rstrip()}')
            else:
                self.out.write(f'\n{pp.rstrip()}')


    def __call__(self):
        for i, line in enumerate(self.inp):
            self._preprocess(i, line)


class Application(object):
    def __init__(self):
        self.source = sys.argv[1]
        self.target = sys.argv[2]

    def main(self):
        with Preprocessor(self.source, self.target) as pp:
            pp()

Application().main()

