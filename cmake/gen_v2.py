#!/usr/bin/env python3
import re, os, sys

class Preprocessor(object):
    def __init__(self, app):
        self.inp = app.source
        self.out = app.target

    def __enter__(self):
        self.inp = open(self.inp, 'r')
        self.out = open(self.out, 'w')
        return self

    def __exit__(self, *args):
        self.inp.close()
        self.out.close()

    @classmethod
    def _format(cls, text):
        return re.sub(r'\{%[ ]*(.+?)[ ]*%\}', ')*" << \\1 << R"*(', text)

    def __call__(self):
        inp = self.inp.read()
        cpp = 0
        hpp = 0

        fn = r'\[\[render\]\]\s+([_a-z,A-Z,0-9]*)\('
        st = r'<%'
        py = r'<python?%'
        js = r'<javascript?%'

        matches = list()
        for r in [fn, st, py, js]:
            matches.extend([(m, r) for m in re.finditer(r, inp)])
        matches.sort(key=lambda x: x[0].start())

        for m, r in matches:
            self.out.write(inp[cpp:m.start()])
            if r == fn:
                self.out.write(f'void {m.group(1)}(std::ostream &stream')
                cpp = m.end()
                while inp[cpp].isspace():
                    cpp += 1
                if inp[cpp] != ')':
                    self.out.write(', ')
                cpp = m.end()
            elif r == st or r == py or r == js:
                self.out.write('stream << R"*(')
                cpp = m.end()
                m = re.search(r'%>', inp[cpp:])
                idx = m.start() - 1
                while idx > 0 and inp[idx] != '\n':
                    idx -= 1
                text = inp[cpp:cpp + m.start()]
                text = text.lstrip('\n')
                if text[-2:] == '\n\n':
                    text = text.rstrip() + '\n'
                else:
                    text = text.rstrip()
                self.out.write(self._format(text))
                self.out.write(')*" "\\n";')
                cpp += m.end()

        self.out.write(inp[cpp:])


class Application(object):
    def __init__(self):
        self.source = sys.argv[1]
        self.target = sys.argv[2]

    def main(self):
        with Preprocessor(self) as pp:
            pp()

Application().main()
