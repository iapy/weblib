# Abstract

The `weblib` library is composed of several key elements:

1. A [metacg](https://bitbucket.org/iapy/metacg) component that incorporates the [crow](https://crowcpp.org/) library to facilitate the creation of synchronous web servers.
2. A metacg module that employs `boost::beast` and `boost::asio` for the development of asynchronous web servers.
3. A collection of classes designed for constructing web service clients, utilizing `boost::beast` as the underlying framework.

`weblib` requires [msgbus](https://bitbucket.org/iapy/msgbus), [config](https://bitbucket.org/iapy/config) and optionally [logger](https://bitbucket.org/iapy/logger) frameworks.

# Synchronous web servers

Initially, we will set up a basic web server that simply starts and listens on a specified port. Over time, we will progressively develop this into a fully operational web server.
```c++
#inclue <weblib/server.hpp>
#inclue <metacg/server.hpp>

int main()
{
	using G = cg::Graph<weblib::Server>();
	return msgbus::Service<Graph>(
	    cg::Args<weblib::Server>(
	        "127.0.0.1", 3000, 1, false
	    )
	);
}
```

The code snippet provided initiates a web server and associates it with the IP address `120.0.0.1:3000`. This web server utilises a single thread (as indicated by the third argument) to handle requests and does not generate any logs (as specified by the fourth argument).

The `weblib::Server` component is configurable via `JSON`:

```json
{
	"weblib::Server": {
		"addr": "127.0.0.1",
		"port": 300,
		"threads": 12,
		"enable_logger": false
	}
}
```

In the subsequent phase of our web server development, we will code a request handler:

```c++
#include <weblib/tags.hpp>
#include <cg/component.hpp>
#include <cg/bind.hpp>

struct Handler
{
	struct Impl
	{
		template<typename Base>
		struct Interface : cg::Bind<Interface, Base>
		{
			void setup(auto &app)
			{
				using Self = Interface<Base>;
				CROW_ROUTE(app, "/hello")(this->bind(&Self::hello));
			}
		private:
			std::string hello()
			{
				return "Hello world!";
			}
		};
	};

	using Ports = ct::map<ct::pair<weblib::tag::Handler, Impl>>;
};
```

The code above creates `metacg` component capable of handling `GET` requests directed at the `/hello` path. To make use of this component, it should be connected to `weblib::Server` as follows:

```c++
using G = cg::Graph<cg::Connect<weblib::Server, Handler>>();
```

When your code includes several components to manage requests, as is common with most web servers, the following shortcut can be utilized:

```c++
using G = cg::Graph<weblib::Handlers<
	Handler, AnotherHandler, ThirdHandler
>>();
```

For understanding the `CROW_ROUTE` macro, the implementation of request handling and signatures of request handlers in [crow](https://crowcpp.org/), please refer to their documentation.

## Blueprints

The [crow](https://crowcpp.org/) framework supports  blueprints (for more details, see their documentation). In the `weblib`framework, it’s assumed that each component providing a request handler can define multiple blueprints. The code snippet below illustrates a component that defines a single blueprint:

```c++
#include <weblib/router.hpp>

struct Handler
{
	struct Impl
	{
		template<typename Base>
		struct Interface : cg::Bind<Interface, Base>
		{
			void setup(auto &app)
			{
				using Self = Interface<Base>;
				WEBLIB_BP_ROUTE("/")(this->bind(&Self::hello));
				app.register_blueprint(this->state.blueprint);
			}
		private:
			std::string hello()
			{
				return "Hello world!";
			}
		};
	};

	template<typename>
	struct State
	{
		crow::Blueprint blueprint{"hello"};
	};

	using Ports = ct::map<ct::pair<weblib::tag::Handler, Impl>>;
};
```

Above code is equivalent to previously defined handler. Note the macro `WEBLIB_BP_ROUTE` that wraps `CROW_BP_ROUTE` passing `State::blueprint`.

The following snippet demonstrates an uncommon scenario where a component needs to have several blueprints. This is particularly beneficial when a component has a private web interface that is only available from the local machine, and a public web interface that can be reached externally through proxies like `nginx` or `apache`:

```c++
#include <weblib/router.hpp>

struct Handler
{
	struct Impl
	{
		template<typename Base>
		struct Interface : cg::Bind<Interface, Base>
		{
			void setup(auto &app)
			{
				using Self = Interface<Base>;
				WEBLIB_BP_ROUTE("/")(this->bind(&Self::hello));
				WEBLIB_BP_ROUTE(internal, "/")(this->bind(&Self::internal));
				app.register_blueprint(this->state.blueprint);
				app.register_blueprint(this->state.internal);
			}
		private:
			std::string hello()
			{
				return "Hello world!";
			}
			std::string internal()
			{
				return "Hi there, sup?";
			}
		};
	};

	template<typename>
	struct State
	{
		crow::Blueprint blueprint{"hello"};
		crow::Blueprint internal{"private"};
	};

	using Ports = ct::map<ct::pair<weblib::tag::Handler, Impl>>;
};
```

Observe that the `WEBLIB_BP_ROUTE` macro, when given only one argument (path), defaults to using `State::blueprint`. However, when provided with two arguments, the first is the name of the field in `State`and the second is the path. Therefore, `WEBLIB_BP_ROUTE("/")` is equivalent to `WEBLIB_BP_ROUTE(blueprint, "/")`.

## Logging

The `logger::Logger` component can be linked to the `weblib::Server` in order to reroute the `crow` log to the `logger` sink:

```c++
using Graph = cg::Graph
<
	cg::Connect<weblib::Server, logger::Logger>,
	cg::Connect<logger::Logger, msgbus::Msgbus>
>;
```

This also introduces a `logger` blueprint with the following request handlers:

- `GET /logger` returns `true` if the `crow` log is being redirected to `logger::Logger`, and `false` if not.
- `POST /logger/1` activates the redirection of `crow` logging to `logger::Logger`.
- `POST /logger/0` deactivates the redirection of `crow` logging to `logger::Logger`.
- `PUT /logger` rotates the log (i.e., it calls the `rotate()` method of `logger::Logger`) and replies with the path to the newly generated file.

## Middlewares

The `weblib` framework is compatible with [crow](https://crowcpp.org/)'s middlewares (for more information, please consult their documentation). A middleware can be defined by any component in the following manner:

```c++
struct Handler
{
	template<typename>
	struct Types
	{
		struct Middleware : crow::CookieParser
		{
			void before_handle(crow::request&, crow::response&, context&)
			{
			}
			void after_handle(crow::request&, crow::response&, context&)
			{
			}
		};
	};

	// the rest of the component's code
};
```

There’s no need for extra initialisation because the `weblib::Server` component will automatically scan all connected handlers for presence of `Middleware` class in `Types` during compile-time and build a suitable middleware chain.

# Template engine

The `weblib` library provides a quick template engine. This template engine uses a very straightforward syntax, as shown below:

```c++
// page.hxx
[[render]] body(std::string const &name);

// page.cxx
[[render]] body(std::string const &name)
{
<%
<html>
	<head><title>Generated web page</title></head>
	<body>
		Hello, {% name %}
	</body>
</html>
%>
}
```

* The `[[render]]` directive tells page generator to tread this function as page generator
* `<% %>`  tags are used to enclose multiline content.
* `{% %}`  tags are used to enclose multiline content.

The code generator responsible for this template generates the subsequent C++ code:

```c++
void body(std::ostream &stream, std::string const &name)
{
	stream << R"*(<html>
	<head><title>Generated web page</title></head>
	<body>
		Hello, )*" << name << R"*(
	</body>
</html>)*" "\n";
}
```

Within the C++ section of the template code, you can utilize standard C++ constructs such as `for`, `while`, `if`, and other standard C++ operators. This allows you to write typical C++ code. For example:

```c++
// page.hxx
[[render]] body(std::map<std::string, float> const &scores);

// page.cxx
[[render]] body(std::map<std::string, float> const &scores)
{
<%
<html>
	<head><title>Generated web page</title></head>
	<body>
		<table>
%>
	for(auto const &[name, score] : scores)
	{
<%
		<tr>
			<td>{% name %}</td>
			<td>{% std::setw(2) << score %}</td>
		</tr>
%>
	}
<%
		</table>
	</body>
</html>
%>
}
```

To incorporate templates into your project, you should proceed as follows:

1. Make a directory named `/pages` (or any name of your preference) within your project repository.
2. Create files such as `/pages/a.hxx` and `/pages/a.cxx`.
3. Subsequently, by appending the necessary lines to your `CMakeLists.txt`, a static library with the generated templates will be produced:

```cmake
GenerateTemplates(WILDCARD
	"${PROJECT_SOURCE_DIR}/pages/*.?xx" LIBRARY pages VERSION 2
)
```

Alternatively, you can add these line if a shared library is required:

```cmake
GenerateTemplates(WILDCARD
	"${PROJECT_SOURCE_DIR}/pages/*.?xx" SHARED LIBRARY pages VERSION 2
)
```

The handler code can utilize the templates in the following manner:

```c++
#incldue <weblib/render.hpp>
#include <weblib/router.hpp>
#include <pages.hpp>

struct Handler
{
	struct Impl
	{
		template<typename Base>
		struct Interface : cg::Bind<Interface, Base>
		{
			void setup(auto &app)
			{
				using Self = Interface<Base>;
				WEBLIB_BP_ROUTE("/")(this->bind(&Self::hello));
				app.register_blueprint(this->state.blueprint);
			}
		private:
			std::string hello()
			{
				return WEBLIB_RENDER(body)(std::map{
					{"Player 1", 23.1},
					{"Player 2", 21.3},
				});
			}
	};
};
```

It’s important to note that we include `pages.hpp`, which is a name for a generated file, instead of `pages.hxx`.
# Web client

The `weblib` library provides two types of web client implementations: `weblib::Client` and `weblib::Agent`. The `weblib::Client` is designed for communicating with a specific web server, and it doesn’t perform address resolution or URL parsing for each request but keeps web cookies. On the other hand, `weblib::Agent`serves as a universal web client.

Let's define a component that uses `weblib::Client`:

```c++
struct DuckDuckGoClient
{
	struct Impl
	{
		template<typename Base>
		struct Interface : Base
		{
			std::string search(std::string const &query)
			{
				weblib::Client::Parameters params{{"q", query}};
				if(auto response = this->state.ddg.template post<
					weblib::Client::Protocol::Https
				>("/", std::move(params));
				response.code == crow::status::OK)
				{
					return response.body;
				}
				throw std::exception();
			}
		};
	};

	template<typename>
	struct State
	{
		weblib::Client ddg{"duckduckgo.com"};
	};
};
```

In the snippet above we defined a simple duckduckgo.com client.

`weblib::Client` provides the following method templates:

```c++
template
<
	weblib::Client::Protocol P,
	typename R = weblib::Client::Response
>
R get(std::string const &path, int32_t const port = P)
```

- The `weblib::Client::Protocol` can be either `weblib::Client::Http` or `weblib::Client::Https`.
- The `typename R` can be either `weblib::Client::Response` or `crow::Response`. The `crow::Response`is particularly useful when a request originates from a web server and the response needs to be redirected directly to the client without any modifications.
- The `port` defaults to 80 if `weblib::Client::Protocol` is `Http` and to 443 if `weblib::Client::Protocol` is `Https`

```c++
template
<
	weblib::Client::Protocol P,
	typename R = weblib::Client::Response,
	typename Params = weblib::Client::Parameters
>
R post(std::string const &path, Params const &params, int32_t const port 
= P)
R put(std::string const &path, Params const &params, int32_t const port = P)
R patch(std::string const &path, Params const &params, int32_t const port = P)
R del(std::string const &path, Params const &params, int32_t const port = P)
R propfind(std::string const &path, Params const &params, int32_t const port = P)
```

Similar to `weblib::Client::get` with one notable difference `Params` argument. It could either be

* `weblib::Client::Parameters`, which is a `typedef` for `std::unordered_map<std::string, std::string>`. In this case, the body’s content type will be `application/x-www-form-urlencoded`, and the `Parameters` will be encoded as a www form.
- `std::string`, which will be sent to the server with the body’s content type set to `text/plain`.
- For all other cases, the content type will be `application/json`, and an attempt will be made to serialise `params` into a string.

# Web agent

The `weblib::Agent` interface can be described as follows:

```c++
Client::Response get (std::string const &);
Client::Response patch (std::string const &, std::string const &);
Client::Response post (std::string const &, std::string const &);
Client::Response put (std::string const &, std::string const &);
Client::Response del (std::string const &, std::string const &);
```

The first argument represents any given web URL, while the second argument corresponds to the data that is to be transmitted to the server.

Moreover, `weblib::Agent` provides the following setup functions:

```c++
inline void set(std::string const &agent);
inline void tor(boost::asio::ip::tcp::tcp::endpoint const &endpoint)
```


- `set` configures the web agent string (for instance, “Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7)…”)
- `tor` sets an endpoint for the `SOCKS5` _tor_ proxy, which facilitates the resolution of `.onion` addresses and fetching data from `tor` services:

```c++
using namespace boost::asio::ip;
using namespace boost::asio::ip::tcp;

agent.tor(endpoint(ip::address::from_string("127.0.0.1"), 1234));
agent.get("https://duckduckgogg42xjoc72x3sjasowoarfbgcmvfimaftt6twagswzczad.onion");
```

# Asynchronous Web server

Unlike `weblib::Server`, `weblib::async::Server` offers a more basic interface for handling HTTP requests, which requires the developer to implement routing.

To begin with, we create a handler and a graph:

```c++
#include <weblib/async/server.hpp>
#include <cg/graph.hpp>
#include <cg/host.hpp>

struct Handler
{
	template<typename>
	struct Types
	{
		struct Context
		{
		};
	};

	struct Impl
	{
		template<typename Base>
		struct Interface : Base
		{
		};
	};

	using Ports = ct::map<ct::pair<weblib::tag::async::Handler, Impl>>;
};

int main()
{
	using G = cg::Graph<
		cg::Connect<weblib::async::Server, Handler>
    >;
    auto host = cg::Host<Graph>(
	    cg::Args<weblib::async::Server>("127.0.0.1", 3000)
    );
    return cg::Service(host);
}
```

The key distinction between `weblib::Server` and `weblib::async::Server` is that the latter only accepts two parameters: the bind address and port. It operates exclusively on a single thread and lacks any logging capabilities. In addition, it *requires* one and only `Handler` which provides optional `Context` type, which the `Handler`can utilise to maintain data related to the request context. Since `weblib::async::Server` is single threader, I can be hosted in `cg::Service` and doesn't require `msgbus::Service`.

Let’s dive into writing the request handler code:

```c++
template<typename Base>
struct Interface : Base
{
	template<typename Context>
	void handle(weblib::async::method::get, Context context)
	{
		auto stream = context->stream();
		stream << "Hello, world!";
		context->status(net::http::status::ok);
	}
};
```

The code provided above will return a `Hello, world!` message to the client for any `GET` request it receives. If the server receives any other HTTP methods, it will respond with `405 Method Not Allowed`. The `context` parameter is a `std::shared_ptr<C>`, where `C` is a subclass of the `Context` type supplied by the handler (or `std::monostate` if the handler does not provide any context). This implements the following interface:

```c++
struct Context
{
// Request
	auto target() const;
	auto method() const;
	auto const &headers() const;
	const char *header(boost::beast::http::field) const;
	std::optional<std::uint64_t> length() const;
	std::optional<std::uint64_t> expected_length() const;

// Response
	auto stream();
	void finish();
	void length(std::size_t);
	void status(boost::beast::http::status);
	void header(boost::beast::http::field, char const *);
	void header(boost::beast::http::field, std::string const &);
	void header(boost::beast::http::field, std::string_view const &);
};
```

The first set of methods are used to parse requests, and the second set of methods are used to build responses:

Request:

- `target()` gives back a `boost::string_view` that contains the requested URL
- `method()` gives back the request method as a `boost::beast::http::verb`
- `header()` gives back the HTTP header value as a raw C string or NULL if it doesn’t exist
- `headers()` gives back a `std::map<boost::beast::http::field, const char*>` that contains the HTTP headers
- `length()` gives back the value of `boost::beast::http::field::content_length` if it exists, and `std::nullopt` if it doesn’t
- `expected_length()` gives back the value of `X-Expected-Entity-Length` if it exists, and `std::nullopt`if it doesn’t

Response:

- `stream()` gives back the `std::ostream` of the response
- `finish()` finishes processing the request
- `length(std::size_t)` sets the length of the response content
- `status(http::status)` sets the status code of the response
- `header(http::field,...)` sets the header of the response

Additionally, `Context` offers a family of `body` and `send` methods for the asynchronous handling of request and response bodies respectively.

Example:

```c++
struct Handler
{
	template<typename>
	struct Types
	{
		struct Context
		{
			auto net() { return net::asio::buffer(buf); }
			auto const &raw() { return buf; }
		private:
			static constexpr std::size_t SIZE = 2 * 1024 * 1024;
			std::array<char, SIZE> buf;
		};
	};

	struct Impl
	{
		template<typename Base>
		struct Interface : Base
		{
			template<typename Context>
			void handle(weblib::async::method::post, Context context)
			{
				context->body(context->net(),
					[context](auto ec, auto sz) -> bool
					{
						auto const raw = context->raw();
						context->stream().write(raw.data(), cn);
						if(ec == net::http::error::end_of_stream)
						{
							context->status(net::http::status::ok);
							context->finish();
						}
					}
					return true;
				);
			}
		};
	};

	using Ports = ct::map<ct::pair<weblib::tag::async::Handler, Impl>>;
};
```

The above is an example of a streaming echo server. It asynchronously accepts any data as a POST request body and synchronously streams that data back to the client. Here’s an explanation of what’s happening in the code:

- The `Context` type is defined for processing requests. It has a 2 mb buffer for processing requests and two methods, `raw()` and `net()`, that return the raw (suitable for use in client code) and network buffers respectively.
- The `context->body` call initiates the receipt of the request body from the client. The second argument is a callback for received data that is passed two arguments: the error code `ec` and the size of the received buffer `sz`. The received data will be located in the first `sz` bytes of the buffer.
- If `ec == net::http::error::end_of_stream`, it indicates that the client has stopped sending data and we can respond.
- The `context->finish()` call is necessary because the request is being processed asynchronously, and it’s not possible to reliably determine when the processing has been completed.
- The callback always returns `true`. However, it may return `false` if there’s an error, and this would signal that the connection should be ended.

As another example, we will consider a server that sends file asynchronously to the client:

```c++
struct Handler
{
	template<typename>
	struct Types
	{
		struct Context
		{
			auto net() {
				return net::asio::buffer(&data[0], data.size());
			}
			std::vector<char> data;
		};
	};

	struct Impl
	{
		template<typename Base>
		struct Interface : Base
		{
			template<typename Context>
			void handle(weblib::async::method::get, Context context)
			{
				std::string const fname{"/opt/var/data.bin"};

				context->data.resize(std::filesystem::file_size(fname));
				std::ifstream(fname).read(
					&context->data[0], context->data.size());

				context->status(net::http::status::ok);
				context->send(context->net(), [context](auto...){
					context->finish();
				});
			}
		};
	};

	using Ports = ct::map<ct::pair<weblib::tag::async::Handler, Impl>>;
};
```